# Codavel's Network Emulator

## Overview


Codavel's Network Emulator is a tool to emulate network conditions. By applying network rules to incoming and outgoing traffic 
on the machine where the emulator is running, developers can use the tool to quickly test the behaviour of their application under different 
network conditions.

The Emulator is designed to be ran either in a machine that routes the traffic to a service or in the service's machine itself. Given that the emulator 
applies conditions to all the traffic that goes through the machine, it is recommended this tool to be used
in a test environment or inside a container.

Currently, this tool allows the configuration of the following network parameters
* Bandwidth Limit
* Latency
* Packet Loss


## Architecture

Codavel's Network Emulator is composed by following components:
* [`emulator`](core): The Emulator service responsible for applying network rules. To interact with it, the service listens for connections on an IPC socket.
* [`emulator-cli`](core/cli): A command line interface used to communicate with the emulator service.
* [`emulator-api`](rest_api): A Rest API used to communicate with with the emulator service.
* [`emulator-web-interface`](rest_api): A simple web interface that communicates with the emulator api for an easier configuration of the emulator service.
 

## Requirements

The current version of Codavel's Network Emulator relies on Linux's netem module (https://wiki.linuxfoundation.org/networking/netem) and its command line tool _tc_ to apply the intended network rules.
Thus, this tool is currently only available on Linux systems (see [container](container) for running on a docker container).

The remaining requirements are:
* C++11 compiler and CMake, to build the core component and the CLI
* NodeJS v8 or above, for building and running the API and Web interface


## Building

The network emulator is mainly divided into two components: the emulator core (with its command line interface) developed in c++, and the Rest API (shipped with a web interface) developed in NodeJS and HTML+CSS+Javascript.

To build the core enter the root folder of this project and run:

``` bash
cmake . && make && make install
```

This will compile the core and cli, and install the binaries for the emulator core and cli, and the necessary headers files on the folder network_emulator.

To build the Rest API enter the folder network_emulator and run:

``` bash
npm install;
npm run build;
```

It is required to run the script _build_, since the REST API relies on c++ modules to communicate with the emulator service. The build script will compile necessary native addon modules.


## Running

Codavel's Network emulator to be used in 2 ways:
* through a command line interface (CLI)
* through a Rest API.


### CLI

To use the CLI, first start the service (please note that it needs to run in privileged mode):

``` bash
sudo ./src/daemon/build/network_emulator_daemon; 
```

Then, to interact with the daemon use the CLI executable (please see [`emulator-cli`](core/cli) for more instructions):

``` bash
network_emulator MODE [OPTIONS]
```

### REST API

To use the REST API, enter the folder network_emulator and run:
``` bash
sudo npm start
```
 
This will start the REST API on port 8888 (please see [`emulator-api`](rest_api) for more instructions) and the emulator service. If you only want to start the API (and start the emulator through the daemon) run:

``` bash
npm run start-api-only
```

After starting the API, the _Configuration Webpage_ is acessible through the URL:

```http
http://<machine_ip>:8888/configuration.html 
```

![Configuration web page](web_iface.png)

 

