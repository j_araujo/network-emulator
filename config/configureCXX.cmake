set(CMAKE_CXX_STANDARD 11)

if (APPLE)
    set(CMAKE_MACOSX_RPATH 1)
endif ()

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3 -std=c++11 -fPIC -fno-omit-frame-pointer")
