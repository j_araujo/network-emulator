cmake_minimum_required(VERSION 3.9)

set(target emulator)

set(CMAKE_EXPORT_COMPILE_COMMANDS 1)

######################################
########## COMMONS LIBRARY ###########
######################################

file(GLOB_RECURSE INCS "${CMAKE_CURRENT_SOURCE_DIR}/include/*.h" "${CMAKE_CURRENT_SOURCE_DIR}/include/*.hpp")
file(GLOB_RECURSE SRCS "${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp" "${CMAKE_CURRENT_SOURCE_DIR}/src/*.h" )

add_library(${target} ${SRCS} ${INCS})

list(REMOVE_ITEM EMULATOR_LIBRARIES "${target}")

target_link_libraries(${target}
        ${EMULATOR_LIBRARIES})

# add the include dir as a public include directory
# will be inherited by dependent targets
# add the include directory src which will only be visible by
# sample_exe
target_include_directories(${target}
        PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include
        PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src)

set_target_properties(${target}
        PROPERTIES
        ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/build"
        LIBRARY_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/build"
        RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/build"
        PUBLIC_HEADER  "${INCS}"

        )

INSTALL(TARGETS ${target}
        LIBRARY DESTINATION "${CMAKE_SOURCE_DIR}/build/lib"
        ARCHIVE DESTINATION "${CMAKE_SOURCE_DIR}/build/lib"
        PUBLIC_HEADER DESTINATION "${CMAKE_SOURCE_DIR}/build/include"
        )

#################################
############ TESTS  #############
#################################


find_package(GTest REQUIRED)
if (GTEST_FOUND)
    include_directories(${GTEST_INCLUDE_DIRS})
endif ()

file(GLOB_RECURSE TEST_HEADER_FILES "${CMAKE_CURRENT_SOURCE_DIR}/tests/*.h")
file(GLOB_RECURSE TEST_SRC_FILES "${CMAKE_CURRENT_SOURCE_DIR}/tests/*.cpp")

enable_testing()

add_executable(${target}_test ${TEST_SRC_FILES} ${TEST_HEADER_FILES})

target_link_libraries(${target}_test
        gtest
        gtest_main
        ${target}
        ${EMULATOR_LIBRARIES})

target_include_directories(${target}_test
        PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include
        PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src)

set_target_properties(${target}_test
        PROPERTIES
        ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/lib"
        LIBRARY_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/lib"
        RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/bin/tests"
        )

gtest_add_tests(${target}_test "" AUTO)



