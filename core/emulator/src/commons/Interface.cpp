/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

#include <fstream>
#include <iostream>
#include <Interface.h>
#include <Emulator.h>
#include <Config.h>

namespace network_emulator {
namespace commons {

Interface::Interface(nlohmann::json input_json) {

    if (input_json.find(JSON_IFACE) != input_json.end()) {
        _physical_iface = input_json[JSON_IFACE].get<std::string>();
        if (TRACE) {
            std::cout << "Parsing interface " << _physical_iface << std::endl;
        }
    }

    if (input_json.find(JSON_INCOMING) != input_json.end()) {
        if (TRACE) {
            std::cout << "Parsing incoming" << std::endl;
        }

        _incoming_rules = new Rules(input_json[JSON_INCOMING]);
    }

    if (input_json.find(JSON_OUTGOING) != input_json.end()) {
        if (TRACE) {
            std::cout << "Parsing outgoing" << std::endl;
        }

        _outgoing_rules = new Rules(input_json[JSON_OUTGOING]);
    }
}

nlohmann::json Interface::toJson() {

    nlohmann::json rules_json;
    rules_json[JSON_IFACE] = _physical_iface;

    if (TRACE) {
        std::cout << "Interface to json" << std::endl;
    }

    if (getIncomingRules() != nullptr) {
        rules_json[JSON_INCOMING] = getIncomingRules()->toJson();
    } else {
        rules_json[JSON_INCOMING] = Rules(0, 0, 0).toJson();
    }

    if (getOutgoingRules() != nullptr) {
        rules_json[JSON_OUTGOING] = getOutgoingRules()->toJson();
    } else {
        rules_json[JSON_OUTGOING] = Rules(0, 0, 0).toJson();
    }

    return rules_json;
}

Interface Interface::loadJson(std::string json_path) {

    std::ifstream ifs(json_path);
    Interface iface = Interface();

    try {
        nlohmann::json j = nlohmann::json::parse(ifs);
        iface = Interface(j);
        if (TRACE) {
            std::cout << "Iface " << iface.getPhysicalIface() << std::endl;
        }
    } catch (nlohmann::json::parse_error &e) {
        if (TRACE) {
            std::cout << "Error parsing json" << std::endl;
        }
    }

    return iface;
}
}
}