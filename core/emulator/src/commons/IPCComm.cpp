/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

#include "IPCComm.h"
#include "Config.h"

#define DEFAULT_ERROR_MESSAGE "{\"STATUS\": \"ERROR\"}"
#define BUFFER_SIZE 4096

namespace network_emulator {

namespace commons {

std::string IPCComm::sendMessageToEmulator(const char *address, const char *message) {

    int emulator_sock = IPCComm::connectToIPCSocket(address);
    if (emulator_sock == -1) {
        return DEFAULT_ERROR_MESSAGE;
    }

    int bytes_sent = (int) send(emulator_sock, message, (strlen(message) + 1), 0);
    if (bytes_sent <= 0) {
        return DEFAULT_ERROR_MESSAGE;
    }

    return IPCComm::receiveMessage(emulator_sock);
}

int IPCComm::connectToIPCSocket(const char *address) {

    int sock_fd = socket(PF_UNIX, SOCK_STREAM, 0);
    struct sockaddr_un remote_addr;

    memset(&remote_addr, 0, sizeof(remote_addr));
    remote_addr.sun_family = AF_UNIX;
    strcpy(remote_addr.sun_path, address);

    if (connect(sock_fd, (struct sockaddr *) &remote_addr, sizeof(remote_addr)) == -1) {
        std::cout << "Couldn't connect to daemon" << std::endl;
        return -1;
    }

    return sock_fd;
}

std::string IPCComm::receiveMessage(int socket_fd) {

    char rcv_buffer[BUFFER_SIZE];

    ssize_t bytes_read = recv(socket_fd, rcv_buffer, BUFFER_SIZE, 0);
    rcv_buffer[bytes_read] = 0;
    std::string result(rcv_buffer);

    if (TRACE) {
        std::cout << "Received " << bytes_read << " bytes:\n" << result << std::endl;
    }

    return result;
}

int IPCComm::createIPCServerSocket(const char *sock_address) {

    /**
     * server socket data structure
     */
    struct sockaddr_un sock;
    memset(&(sock), 0, sizeof(struct sockaddr_un));

    unlink(sock_address);
    int sock_fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (sock_fd < 0) {
        return -1;
    }

    sock.sun_family = AF_UNIX;
    strcpy(sock.sun_path, sock_address);
    int res = bind(sock_fd, (struct sockaddr *) &(sock), sizeof(struct sockaddr_un));
    if (res < 0) {
        return -1;
    }

    listen(sock_fd, 1);

    chmod(sock_address, (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IRWXO));
    /*
     * Make socket non-blocking so that we don't hang forever if
     * target dies unexpectedly.
     */
    int flags = fcntl(sock_fd, F_GETFL);
    if (flags >= 0) {
        flags |= O_NONBLOCK;
        fcntl(sock_fd, F_SETFL, flags);
    }

    return sock_fd;
}
}
}
