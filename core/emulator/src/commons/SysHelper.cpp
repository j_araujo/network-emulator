/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

#include "SysHelper.h"
#include "Config.h"

namespace network_emulator {
namespace commons {

int SysHelper::systemCall(char *args[], std::string *result) {

    int wait_status, call_pipe[2], i = 1;
    struct pollfd pollfd[1];
    pid_t pid;

    if (TRACE) {
        std::cout << "Exec " << args[0] << std::endl;
        int k = 1;
        while (args[k] != nullptr) {
            std::cout << "\t" << args[k++] << std::endl;
        }
    }

    if (pipe(call_pipe) == -1) {
        return -1;
    }

    pid = fork();
    if (pid < 0) {
        return -1;
    }

    if (pid == 0) {
        /* child process */
        dup2(call_pipe[1], STDOUT_FILENO);
        dup2(call_pipe[1], STDERR_FILENO);
        close(call_pipe[0]);

        /* if execvp fails, exits child process */
        execvp(args[0], args);
        _exit(-1);
    }

    close(call_pipe[1]);
    pollfd[0].fd = call_pipe[0];
    pollfd[0].events = POLLIN | POLLERR | POLLHUP;
    pollfd[0].revents = 0;

    while (true) {
        char buffer[1024];
        ssize_t res;
        int n;

        n = poll(pollfd, 1, 1000);
        if (n <= 0) {
            if (TRACE) {
                std::cout << args[0] << "timeout" << std::endl;
            }

            kill(pid, 9);
            break;
        }

        if (pollfd[0].revents & POLLERR || pollfd[0].revents & POLLHUP) {
            break;
        }

        res = read(pollfd[0].fd, buffer, 1024);
        if (res > 0) {
            buffer[res] = 0;

            if (result != nullptr) {
                *result += buffer;
            }
        }

        pollfd[0].revents = 0;
    }

    close(call_pipe[0]);
    waitpid(pid, &wait_status, 0);

    if (result != nullptr && TRACE) {
        std::cout << *result << std::endl;
    }

    return wait_status;
}
}
}

