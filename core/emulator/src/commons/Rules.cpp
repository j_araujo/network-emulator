/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

#include "Rules.h"
#include <Emulator.h>

namespace network_emulator {
namespace commons {

Rules::Rules(nlohmann::json input_json) {

    if (input_json.find(JSON_PACKET_LOSS) != input_json.end()) {
        _packet_loss = input_json[JSON_PACKET_LOSS].get<float>();
    }

    if (input_json.find(JSON_LATENCY) != input_json.end()) {
        _latency = input_json[JSON_LATENCY].get<int>();
    }

    if (input_json.find(JSON_RATE_LIMIT) != input_json.end()) {
        _rate = input_json[JSON_RATE_LIMIT].get<int>();
    }
}

nlohmann::json Rules::toJson() {

    nlohmann::json rules_json;
    rules_json[JSON_PACKET_LOSS] = _packet_loss;
    rules_json[JSON_LATENCY] = _latency;
    rules_json[JSON_RATE_LIMIT] = _rate;
    return rules_json;
}
}
}