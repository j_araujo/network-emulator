/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

#pragma once

#include <string>
#include <vector>
#include <poll.h>
#include <iostream>
#include "SysHelper.h"
#include <ifaddrs.h>
#include <iostream>
#include <sys/socket.h>
#include <zconf.h>
#include <signal.h>
#include <net/if.h>

#ifdef __linux__
#include <wait.h>
#endif

namespace network_emulator {
namespace commons {

/**
 * Responsible for dealing with any system specific methods
 */

class SysHelper {
  public:
    /**
     * Creates a process and executes a process on the operating system, returning the results
     *
     * @param args array with the command (arg[0]) and all the necessary arguments
     * @param result output param, where the result will be written to
     * @return 0 if the command exited successfully
     */
    static int systemCall(char *args[], std::string *result);
};
}
}
