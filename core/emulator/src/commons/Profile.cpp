/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

#include "Profile.h"
#include <Rules.h>
#include <stdio.h>
#include <dirent.h>
#include <string>
#include <iostream>
#include <fstream>
#include <Emulator.h>
#include <Config.h>

namespace network_emulator {
namespace commons {

#define SPACE ' '
#define UNDERSCORE '_'
#define SLASH "/"
#define EXTENSION ".json"

Profile Profile::loadFromJSON(nlohmann::json json) {

    if (json.find(JSON_NAME) == json.end()) {
        Profile p;
        return p;
    }

    Profile profile(json[JSON_NAME].get<std::string>());
    if (TRACE) {
        std::cout << "Parsing profile " << std::endl;
    }

    if (json.find(JSON_INCOMING) != json.end()) {
        if (TRACE) {
            std::cout << "Parsing incoming" << std::endl;
        }

        profile.setIncomingRules(new Rules(json[JSON_INCOMING]));
    }

    if (json.find(JSON_OUTGOING) != json.end()) {
        if (TRACE) {
            std::cout << "Parsing outgoing" << std::endl;
        }

        profile.setOutgoingRules(new Rules(json[JSON_OUTGOING]));
    }

    return profile;
}

/**
 * Loads a new Profile from a file path
 *
 * @param path the path of the folder where the profiles are stored
 * @return a profile
 */
static Profile loadFromFile(std::string path) {

    std::ifstream ifs(path);
    Profile profile;

    try {
        if (TRACE) {
            std::cout << "Loading json from " << path << std::endl;
        }

        nlohmann::json j = nlohmann::json::parse(ifs);
        profile = Profile::loadFromJSON(j);

        if (TRACE) {
            std::cout << "Profile " << profile.getName() << std::endl;
        }
    } catch (nlohmann::json::parse_error &e) {
        if (TRACE) {
            std::cout << "Error parsing json" << std::endl;
        }
    }

    return profile;
}

void Profile::saveToFile(std::string folder) {

    const char *name = _name.c_str();
    char filename[_name.length()];

    int i = 0, j = 0;
    while (name[i] != 0) {
        if (name[i] == SPACE) {
            filename[j++] = UNDERSCORE;
        } else {
            filename[j++] = name[i];
        }

        i++;
    }

    filename[j] = 0;

    std::string profile_filename = folder + SLASH + std::string(filename) + EXTENSION;
    std::ofstream file(profile_filename);
    file << toJson();
}

std::vector<Profile> Profile::loadProfiles(std::string folder) {

    if (TRACE) {
        std::cout << "Loading profiles from  " << folder << std::endl;
    }

    std::vector<Profile> results;

    DIR *dirp = opendir(folder.c_str());
    if (dirp == nullptr) {
        if (TRACE) {
            std::cout << "Folder " << folder << " does not exist" << std::endl;
        }

        return results;
    }

    struct dirent *dp;
    std::string suffix(EXTENSION);
    while ((dp = readdir(dirp)) != nullptr) {
        std::string filename(dp->d_name);

        if (filename.length() >= suffix.length()) {
            if ((filename.compare(filename.length() - suffix.length(), suffix.length(), suffix)) == 0) {
                if (TRACE) {
                    std::cout << "Found " << filename << std::endl;
                }

                Profile profile = loadFromFile(folder + SLASH + filename);
                if (!profile.getName().empty()) {
                    results.emplace_back(profile);
                }
            }
        }
    }

    if (TRACE) {
        std::cout << "Done" << std::endl;
    }

    closedir(dirp);
    return results;
}

nlohmann::json Profile::toJson() {

    nlohmann::json rules_json;
    rules_json[JSON_NAME] = _name;

    if (getIncomingRules() != nullptr) {
        rules_json[JSON_INCOMING] = getIncomingRules()->toJson();
    } else {
        rules_json[JSON_INCOMING] = network_emulator::commons::Rules(0, 0, 0).toJson();
    }

    if (getOutgoingRules() != nullptr) {
        rules_json[JSON_OUTGOING] = getOutgoingRules()->toJson();
    } else {
        rules_json[JSON_OUTGOING] = network_emulator::commons::Rules(0, 0, 0).toJson();
    }

    return rules_json;
}
}
}

