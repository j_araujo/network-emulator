/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

#pragma once

#include <string.h>
#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>

namespace network_emulator {
namespace commons {

/**
 * Handles the IPC communication
 */
class IPCComm {
  public:
    /**
     * Creates an IPC socket
     *
     * @param sock_address the address where the service will listen to messages
     * @return socket file descriptor
     */
    static int createIPCServerSocket(const char *sock_address);

    /**
     * Connects to an IPC socket
     *
     * @param address path of the IPC socket to connect to
     * @return a file descriptor for the socket connected to the address; -1 if an error occurred
     */
    static int connectToIPCSocket(const char *address);

    /**
     * Sends a message to the network emulator and waits for the reply
     *
     * @param address the ipc path where the emulator is listening for messages
     * @param message the message to be sent, in a json format
     * @return a string with the emulator's reply
     */
    static std::string sendMessageToEmulator(const char *address, const char *message);

    /**
     * Waits for a message on a socket
     *
     * @param sock_fd the file descriptor to read the message from
     * @return the message received
     */
    static std::string receiveMessage(int sock_fd);
};
}
}