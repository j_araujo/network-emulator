/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

#include "Emulator.h"
#include "Config.h"

#include <vector>
#include <string.h>
#include <iostream>
#include <sys/un.h>
#include <fcntl.h>
#include <unistd.h>
#include <poll.h>
#include "commons/IPCComm.h"
#include <emulators/netem/Netem.h>

/**
 * Netem emulator
 */
network_emulator::netem::Netem _netem;

#define BUFFER_SIZE 4096

int
network_emulator::Emulator::startEmulator(bool run_background, const char *sock_address, const char *profiles_folder) {

    return startEmulator(run_background, sock_address, profiles_folder, nullptr, nullptr);
}

int
network_emulator::Emulator::startEmulator(bool run_background, const char *sock_address, const char *profiles_folder,
    const char *incoming_interface, const char *outgoing_interface) {

    struct pollfd pollfd[1];
    struct sockaddr_un from;
    socklen_t from_size = sizeof(struct sockaddr_un);
    char rcv_buffer[BUFFER_SIZE];

    std::cout << "Starting emulator" << std::endl;

    if (run_background) {
        int res = daemon(1, 0);
        if (res < 0) {
            std::cout << "Error starting as daemon" << std::endl;
        }
    }

    _sock_fd = network_emulator::commons::IPCComm::createIPCServerSocket(
        sock_address == nullptr ? DEFAULT_IPC_SOCK : sock_address);
    if (_sock_fd < 0) {
        return -1;
    }

    pollfd[0].fd = _sock_fd;
    pollfd[0].events = POLLIN | POLLERR | POLLHUP;
    pollfd[0].revents = 0;

    if (incoming_interface) {
        std::cout << "Starting incoming_interface as " << incoming_interface << std::endl;

        _incoming_interface = std::string(incoming_interface);
    }

    if (outgoing_interface) {
        std::cout << "Starting outgoing_interface as " << outgoing_interface << std::endl;

        _outgoing_interface = std::string(outgoing_interface);
    }

    _profiles = network_emulator::commons::Profile::loadProfiles(
        profiles_folder == nullptr ? DEFAULT_PROFILES_PATH : profiles_folder);
    _netem.start();
    _is_running = true;

    while (_is_running) {
        int i = 0, n;

        n = poll(pollfd, 1, 1000);
        if (n <= 0) {
            continue;
        }

        /* check for new requests from ipc */
        if (pollfd[i].revents & POLLHUP || pollfd[i].revents & POLLERR) {
            std::cout << "Error in socket!" << std::endl;
            _is_running = false;
            continue;
        } else if (pollfd[i].revents & POLLIN) {
            int client_sock = accept(pollfd[i].fd, (struct sockaddr *) &from, &from_size);

            ssize_t bytes_read = recv(client_sock, rcv_buffer, 4096, 0);
            rcv_buffer[bytes_read] = 0;

            if (processMessage(rcv_buffer, client_sock) < 0) {
                std::string error_message = DEFAULT_ERROR_MESSAGE;
                send(client_sock, error_message.c_str(), (strlen(error_message.c_str()) + 1), 0);
            }
        }
    }

    std::cout << "Stopping service" << std::endl;
    _netem.stop();
    return 0;
}

int network_emulator::Emulator::processMessage(char *message, int client_socket_fd) {

    nlohmann::json json;

    try {
        json = nlohmann::json::parse(message);
    } catch (nlohmann::json::parse_error &e) {
        if (TRACE) {
            std::cout << "Error parsing json" << std::endl;
        }        
        return -1;
    }

    if (json.find("COMMAND") == json.end()) {
        return -1;
    }

    auto message_type = json[JSON_COMMAND].get<MessageTypes>();

    bool using_default_ifaces = (!_incoming_interface.empty() || !_outgoing_interface.empty());

    switch (message_type) {
        case GET_RULES: {
            if (TRACE) {
                std::cout << "Received GET_RULES" << std::endl;
            }
            nlohmann::json reply_json;
            auto ifacesJSONS = nlohmann::json::array();

            if (json.find(JSON_IFACE) != json.end()) {
                auto iface_str = json[JSON_IFACE].get<std::string>();
                auto interface_rules = _netem.getInterfaceRules(iface_str);
                ifacesJSONS.push_back(interface_rules.toJson());
            } else {
                auto iface_strs = _netem.getNetworkInterfaces();

                if (using_default_ifaces) {
                    std::string iface = "DEFAULT";
                    network_emulator::commons::Interface default_interface_rules(iface);
                    for (auto iface_str: iface_strs) {
                        auto interface_rules = _netem.getInterfaceRules(iface_str);

                        if (!_incoming_interface.empty() && _incoming_interface == iface_str) {
                            default_interface_rules.setIncomingRules(interface_rules.getIncomingRules());
                        }

                        if (!_outgoing_interface.empty() && _outgoing_interface == iface_str) {
                            default_interface_rules.setOutgoingRules(interface_rules.getOutgoingRules());
                        }
                    }

                    ifacesJSONS.push_back(default_interface_rules.toJson());
                } else {
                    for (auto iface_str: iface_strs) {
                        auto interface_rules = _netem.getInterfaceRules(iface_str);
                        ifacesJSONS.push_back(interface_rules.toJson());
                    }
                }
            }

            reply_json[JSON_INTERFACES] = ifacesJSONS;
            reply_json[JSON_STATUS] = JSON_OK;

            std::string json_res = reply_json.dump();
            const char *rpl = json_res.c_str();
            ssize_t bytes_sent = send(client_socket_fd, rpl, json_res.size(), 0);

            if (TRACE) {
                std::cout << "[GET_RULES] " << bytes_sent << ": " << reply_json.dump() << std::endl;
            }

            return (int) bytes_sent;
        }

        case ADD_RULE: {
            if (TRACE) {
                std::cout << "Received ADD_RULE" << std::endl;
            }

            std::string iface_str;
            int res = 0;

            if (json.find(JSON_RULES) == json.end()) {
                return -1;
            }

            if (json[JSON_RULES].find(JSON_IFACE) == json[JSON_RULES].end() && !using_default_ifaces) {
                return -1;
            }

            if (using_default_ifaces) {
                if (!_incoming_interface.empty() && json[JSON_RULES].find(JSON_INCOMING) != json[JSON_RULES].end()) {
                    auto rules = new network_emulator::commons::Rules(json[JSON_RULES][JSON_INCOMING]);
                    res = _netem.addRule(_incoming_interface, rules, false);
                }

                if (!_outgoing_interface.empty() && json[JSON_RULES].find(JSON_OUTGOING) != json[JSON_RULES].end()) {
                    auto rules = new network_emulator::commons::Rules(json[JSON_RULES][JSON_OUTGOING]);
                    res = _netem.addRule(_outgoing_interface, rules, false);
                }
            } else {
                iface_str = json[JSON_RULES][JSON_IFACE].get<std::string>();

                if (json[JSON_RULES].find(JSON_INCOMING) != json[JSON_RULES].end()) {
                    auto rules = new network_emulator::commons::Rules(json[JSON_RULES][JSON_INCOMING]);
                    res = _netem.addRule(iface_str, rules, true);
                }

                if (json[JSON_RULES].find(JSON_OUTGOING) != json[JSON_RULES].end()) {
                    auto rules = new network_emulator::commons::Rules(json[JSON_RULES][JSON_OUTGOING]);
                    res = _netem.addRule(iface_str, rules, false);
                }
            }

            std::cout << "Rules received: " << json[JSON_RULES].dump() << std::endl;

            const char *rpl;
            if (res == 0) {
                rpl = DEFAULT_OK_MESSAGE;
            } else {
                rpl = DEFAULT_ERROR_MESSAGE;
            }

            if (TRACE) {
                std::cout << "[ADD_RULE]: " << rpl << std::endl;
            }

            ssize_t bytes_sent = send(client_socket_fd, rpl, (strlen(rpl) + 1), 0);
            return (int) bytes_sent;
        }

        case DELETE_RULES: {
            if (TRACE) {
                std::cout << "Received DELETE_RULES" << std::endl;
            }

            int res = 0;

            if (json.find(JSON_IFACE) == json.end() && !using_default_ifaces) {
                return -1;
            }

            if (using_default_ifaces) {
                if (!_incoming_interface.empty()) {
                    res = _netem.removeRulesForInterface(_incoming_interface);
                }

                if (!_outgoing_interface.empty()) {
                    res = _netem.removeRulesForInterface(_outgoing_interface);
                }
            } else {
                auto iface_str = json[JSON_IFACE].get<std::string>();
                res = _netem.removeRulesForInterface(iface_str);
            }

            const char *rpl;
            if (res == 0) {
                rpl = DEFAULT_OK_MESSAGE;
            } else {
                rpl = DEFAULT_ERROR_MESSAGE;
            }

            if (TRACE) {
                std::cout << "[DELETE_RULES]: " << rpl << std::endl;
            }

            ssize_t bytes_sent = send(client_socket_fd, rpl, (strlen(rpl) + 1), 0);
            return (int) bytes_sent;
        }

        case GET_PROFILES: {
            if (TRACE) {
                std::cout << "Received GET_PROFILES" << std::endl;
            }

            auto profilesJSONS = nlohmann::json::array();

            for (network_emulator::commons::Profile p: _profiles) {
                profilesJSONS.emplace_back(p.toJson());
            }

            nlohmann::json reply_json;
            reply_json[JSON_PROFILES] = profilesJSONS;
            reply_json[JSON_STATUS] = JSON_OK;

            std::string json_res = reply_json.dump();
            const char *rpl = json_res.c_str();
            ssize_t bytes_sent = send(client_socket_fd, rpl, json_res.size(), 0);

            if (TRACE) {
                std::cout << "[GET_PROFILES] " << bytes_sent << ": " << reply_json.dump() << std::endl;
            }

            return (int) bytes_sent;
        }

        case SAVE_PROFILE: {
            if (json.find(JSON_PROFILE) == json.end()) {
                return -1;
            }

            auto profile = network_emulator::commons::Profile::loadFromJSON(json[JSON_PROFILE]);
            if (profile.getName().empty()) {
                return -1;
            }

            profile.saveToFile(DEFAULT_PROFILES_PATH);
            const char *rpl = DEFAULT_OK_MESSAGE;

            if (TRACE) {
                std::cout << "[SAVE_PROFILE]: " << rpl << std::endl;
            }

            ssize_t bytes_sent = send(client_socket_fd, rpl, (strlen(rpl) + 1), 0);
            return (int) bytes_sent;
        }

        case SHUTDOWN: {
            _is_running = false;
            return 0;
        }

        default: break;
    }
}

std::string network_emulator::Emulator::sendMessageToEmulator(const char *address, const char *message) {

    return network_emulator::commons::IPCComm::sendMessageToEmulator(address, message);
}
