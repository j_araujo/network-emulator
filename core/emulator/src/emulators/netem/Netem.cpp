/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

#include "Netem.h"
#include "Config.h"
#include <commons/SysHelper.h>
#include <emulators/netem/Parser.h>
#include <iostream>

namespace network_emulator {
namespace netem {

#define NETEM_TC "tc"
#define NETEM_IFB "ifb"
#define NETEM_IP "ip"
#define NETEM_LINK "link"
#define NETEM_SET "set"
#define NETEM_DEV "dev"
#define NETEM_UP "up"
#define NETEM_QDISC "qdisc"
#define NETEM_ADD "add"
#define NETEM_CHANGE "change"
#define NETEM_DEL "del"
#define NETEM_SHOW "show"
#define NETEM_RATE "rate"
#define NETEM_HANDLE "handle"
#define NETEM_FAMILY "ffff:"
#define NETEM_FILTER "filter"
#define NETEM_PARENT "parent"
#define NETEM_PROTOCOL "protocol"
#define NETEM_U32 "u32"
#define NETEM_PARENT_RULE "1:"
#define NETEM_RULE_12 "12"
#define NETEM_RULE_1_1 "1:1"
#define NETEM_RULE_1_12 "1:12"
#define NETEM_0 "0"
#define NETEM_MATCH "match"
#define NETEM_ACTION "action"
#define NETEM_MIRRED "mirred"
#define NETEM_EGRESS "egress"
#define NETEM_INGRESS "ingress"
#define NETEM_REDIRECT "redirect"
#define NETEM_ROOT "root"
#define NETEM_MODPROBE "modprobe"
#define NETEM_R "-r"
#define NETEM_NUM_IFBS "numifbs="
#define NETEM_MBIT "Mbit"
#define NETEM_HTB "htb"
#define NETEM_DEFAULT "default"
#define NETEM_CLASS "class"
#define NETEM_CLASS_ID "classid"
#define NETEM_CEIL "ceil"
#define NETEM_LOSS "loss"
#define NETEM "netem"
#define NETEM_DELAY "delay"
#define NETEM_LIMIT "limit"
#define NETEM_LIMIT_VALUE "1000000"

int Netem::start() {

    stop();
    if (TRACE) {
        std::cout << "Netem: start" << std::endl;
    }

    auto ifaces = getNetworkInterfaces();
    startModProbe((int) ifaces.size());

    return 0;
}

int Netem::stop() {

    if (TRACE) {
        std::cout << "Netem: stopping" << std::endl;
    }

    auto ifaces = getNetworkInterfaces();
    for (auto iface : ifaces) {
        removeInterface(iface);
    }

    stopModProbe();

    return 0;
}

std::string Netem::addVirtualIface(std::string real_iface) {

    unsigned long n_virtual_ifaces = _rules_per_iface->size();
    std::string virtual_iface = NETEM_IFB + std::to_string(n_virtual_ifaces);
    char const
        *add_virtual_iface[] = {NETEM_IP, NETEM_LINK, NETEM_SET, NETEM_DEV, virtual_iface.c_str(), NETEM_UP, nullptr};
    char const *add_ingress_rule[] =
        {NETEM_TC, NETEM_QDISC, NETEM_ADD, NETEM_DEV, real_iface.c_str(), NETEM_HANDLE, NETEM_FAMILY, NETEM_INGRESS,
            nullptr};
    char const *redirect_traffic[] =
        {NETEM_TC, NETEM_FILTER, NETEM_ADD, NETEM_DEV, real_iface.c_str(), NETEM_PARENT, NETEM_FAMILY, NETEM_PROTOCOL,
            NETEM_IP, NETEM_U32, NETEM_MATCH, NETEM_U32, NETEM_0, NETEM_0, NETEM_ACTION, NETEM_MIRRED, NETEM_EGRESS,
            NETEM_REDIRECT, NETEM_DEV, virtual_iface.c_str(), nullptr};

    if (TRACE) {
        std::cout << "Netem: Creating virtual iface for " << real_iface << ": " << virtual_iface << std::endl;
    }

    int res = network_emulator::commons::SysHelper::systemCall((char **) add_virtual_iface, nullptr);
    if (res != 0) {
        return "";
    }

    network_emulator::commons::SysHelper::systemCall((char **) add_ingress_rule, nullptr);
    network_emulator::commons::SysHelper::systemCall((char **) redirect_traffic, nullptr);
    return virtual_iface;
}

int Netem::removeRulesForInterface(std::string iface) {

    char const
        *remove_iface_rules[] = {NETEM_TC, NETEM_QDISC, NETEM_DEL, NETEM_DEV, iface.c_str(), NETEM_ROOT, nullptr};

    if (TRACE) {
        std::cout << "Netem: Removing rules for " << iface << std::endl;
    }

    int res = network_emulator::commons::SysHelper::systemCall((char **) remove_iface_rules, nullptr);
    return res;
}

int Netem::removeInterface(std::string iface) {

    char const *remove_virtual_iface[] =
        {NETEM_TC, NETEM_QDISC, NETEM_DEL, NETEM_DEV, iface.c_str(), NETEM_HANDLE, NETEM_FAMILY, NETEM_INGRESS,
            nullptr};

    if (TRACE) {
        std::cout << "Netem: Removing interface " << iface << std::endl;
    }

    int res = network_emulator::commons::SysHelper::systemCall((char **) remove_virtual_iface, nullptr);
    if (res == 0) {
        removeRulesForInterface(iface);
    }

    _rules_per_iface->erase(iface);
    return res;
}

int Netem::stopModProbe() {

    char const *remove_all_virtual_ifaces[] = {NETEM_MODPROBE, NETEM_R, NETEM_IFB, nullptr};
    int res = network_emulator::commons::SysHelper::systemCall((char **) remove_all_virtual_ifaces, nullptr);
    return res;
}

int Netem::startModProbe(int n_interfaces) {

    stopModProbe();

    std::string n_virtual_ifaces = NETEM_NUM_IFBS + std::to_string(n_interfaces);
    char const *add_virtual_ifaces[] = {NETEM_MODPROBE, NETEM_IFB, n_virtual_ifaces.c_str(), nullptr};
    int res = network_emulator::commons::SysHelper::systemCall((char **) add_virtual_ifaces, nullptr);

    return res;
}

std::vector<std::string> Netem::getNetworkInterfaces() {

    char const *args[] = {NETEM_TC, NETEM_QDISC, NETEM_SHOW, nullptr};
    std::string result;

    int res = network_emulator::commons::SysHelper::systemCall((char **) args, &result);
    if (res != 0) {
        std::vector<std::string> results;
        return results;
    }

    network_emulator::netem::Parser parser;
    return parser.getAvailableInterfaces(result);
}

network_emulator::commons::Interface Netem::getInterfaceRules(std::string iface) {

    auto it = _rules_per_iface->find(iface);
    if (it == _rules_per_iface->end()) {
        network_emulator::commons::Interface interface(iface);
        return interface;
    }

    return _rules_per_iface->at(iface);
}

int Netem::addRule(std::string physical_iface, network_emulator::commons::Rules *rule, bool incoming) {

    network_emulator::commons::Interface interface(physical_iface);
    network_emulator::commons::Rules *existing_rules;
    std::string mode, packet_loss_str, latency_str, iface;

    auto it = _rules_per_iface->find(physical_iface);
    if (it == _rules_per_iface->end()) {
        std::string virtual_iface = addVirtualIface(physical_iface);
        interface.setVirtualIface(virtual_iface);

        if (TRACE) {
            if (virtual_iface.empty()) {
                std::cout << "Error creating virtual iface for " << physical_iface << std::endl;
            } else {
                std::cout << "Adding virtual interface " << virtual_iface << " to " << physical_iface << std::endl;
            }
        }        

        interface.setOutgoingRules(new network_emulator::commons::Rules(0, 0, 0));
        interface.setIncomingRules(new network_emulator::commons::Rules(0, 0, 0));

        std::pair<std::string, network_emulator::commons::Interface> newEntry(physical_iface, interface);
        _rules_per_iface->insert(newEntry);
    }

    interface = _rules_per_iface->at(physical_iface);
    if (incoming) {
        if (interface.getVirtualIface().empty()) {
            if (TRACE) {
                std::cout << "No virtual iface for " << physical_iface << std::endl;
            }
            return -1;
        }

        iface = interface.getVirtualIface();
        existing_rules = interface.getOutgoingRules();
    } else {
        iface = interface.getPhysicalIface();
        existing_rules = interface.getIncomingRules();
    }

    if (TRACE) {
        std::cout << "Netem::addRule:: Adding " << (incoming ? "incoming" : "outgoing") << " rules for " << iface
                << std::endl;

        std::cout << "Netem:: rule->getRate " << rule->getRate() << " | existing_rules->getRate "
                << existing_rules->getRate() << std::endl;
    }

    if (rule->getRate() != existing_rules->getRate()) {
        int res = addRateLimit(&iface, rule, existing_rules);

        if (res != 0) {
            return res;
        }
    }

    if (TRACE) {
        std::cout << "Netem:: rule->getLatency " << rule->getLatency() << " | existing_rules->getLatency "
                << existing_rules->getLatency() << std::endl;

        std::cout << "Netem:: rule->getPacketLoss " << rule->getPacketLoss() << " | existing_rules->getPacketLoss "
                << existing_rules->getPacketLoss() << std::endl;
    }

    if (rule->getLatency() != existing_rules->getLatency()
        || rule->getPacketLoss() != existing_rules->getPacketLoss()) {
        bool root_rule = false;
        if (rule->getRate() <= 0) {
            // no rule for rate limit
            root_rule = true;
        }

        int res = addLatencyAndPacketLoss(&iface, rule, existing_rules, root_rule);
        if (res != 0) {
            return res;
        }
    }

    existing_rules->setRate(rule->getRate());
    existing_rules->setLatency(rule->getLatency());
    existing_rules->setPacketLoss(rule->getPacketLoss());

    return 0;
}

int Netem::addRateLimit(std::string *iface, network_emulator::commons::Rules *new_rule,
    network_emulator::commons::Rules *existing_rule) {

    std::string rate_str = std::to_string(new_rule->getRate()) + NETEM_MBIT;
    std::string mode;
    bool reapply_rules = (existing_rule->getLatency() > 0 || existing_rule->getPacketLoss() > 0);
    int res = 0;

    if (new_rule->getRate() <= 0) {
        removeRulesForInterface(*iface);

        if (reapply_rules) {
            res = addLatencyAndPacketLoss(iface, existing_rule, nullptr, true);
        }

        return res;
    }

    if (existing_rule->getRate() <= 0) {
        if (reapply_rules) {
            removeRulesForInterface(*iface);
        }

        char const *args_class[] =
            {NETEM_TC, NETEM_QDISC, NETEM_ADD, NETEM_DEV, iface->c_str(), NETEM_ROOT, NETEM_HANDLE, NETEM_PARENT_RULE,
                NETEM_HTB, NETEM_DEFAULT, NETEM_RULE_12, nullptr};
        res = network_emulator::commons::SysHelper::systemCall((char **) args_class, nullptr);
        if (res != 0) {
            return res;
        }

        mode = NETEM_ADD;
        if (TRACE) {
            std::cout << "Netem: Adding rate limit of " << rate_str << " to " << *iface << std::endl;
        }        
    } else {
        reapply_rules = false;

        mode = NETEM_CHANGE;
        if (TRACE) {
            std::cout << "Netem: Changing rate limit to " << rate_str << " to " << *iface << std::endl;
        }
    }

    char const *args_rate[] =
        {NETEM_TC, NETEM_CLASS, mode.c_str(), NETEM_DEV, iface->c_str(), NETEM_PARENT, NETEM_RULE_1_1, NETEM_CLASS_ID,
            NETEM_RULE_1_12, NETEM_HTB, NETEM_RATE, rate_str.c_str(), NETEM_CEIL, rate_str.c_str(), nullptr};
    res = network_emulator::commons::SysHelper::systemCall((char **) args_rate, nullptr);
    if (res != 0) {
        return res;
    }

    if (reapply_rules) {
        res = addLatencyAndPacketLoss(iface, existing_rule, nullptr, false);
    }

    return res;
}

int Netem::addLatencyAndPacketLoss(std::string *iface, network_emulator::commons::Rules *new_rule,
    network_emulator::commons::Rules *existing_rule, bool root) {

    int res;

    if (new_rule->getPacketLoss() <= 0 && new_rule->getLatency() <= 0) {
        res = removeLatencyAndPacketLoss(iface, root);
        return res;
    }

    if (new_rule->getLatency() <= 0) {
        return addPacketLoss(iface, new_rule, existing_rule, root);
    } else if (new_rule->getPacketLoss() <= 0) {
        return addLatency(iface, new_rule, existing_rule, root);
    }

    if (TRACE) {
        std::cout << "Netem: adding latency and packet loss to " << *iface << " ( " << (root ? "root" : "child") << " )"
                  << std::endl;
    }

    std::string packet_loss_str = std::to_string(new_rule->getPacketLoss()) + "%";
    std::string latency_str = std::to_string(new_rule->getLatency()) + "ms";
    std::string mode;

    if (existing_rule == nullptr || (existing_rule->getLatency() <= 0 && existing_rule->getPacketLoss() <= 0)) {
        mode = NETEM_ADD;
        if (TRACE) {
            std::cout << "Netem: Adding latency of " << latency_str << " and packet loss of " << packet_loss_str << " to "
                      << *iface << std::endl;
        }
    } else {
        mode = NETEM_CHANGE;
        if (TRACE) {
            std::cout << "Netem: Changing latency to " << latency_str << " and packet loss to " << packet_loss_str << " to "
                      << *iface << std::endl;
        }
    }

    if (root) {
        char const *args[] =
            {NETEM_TC, NETEM_QDISC, mode.c_str(), NETEM_DEV, iface->c_str(), NETEM_ROOT, NETEM, NETEM_DELAY,
                latency_str.c_str(), NETEM_LOSS, packet_loss_str.c_str(), NETEM_LIMIT, NETEM_LIMIT_VALUE, nullptr};
        res = network_emulator::commons::SysHelper::systemCall((char **) args, nullptr);
    } else {
        char const *args[] =
            {NETEM_TC, NETEM_QDISC, mode.c_str(), NETEM_DEV, iface->c_str(), NETEM_PARENT, NETEM_RULE_1_12, NETEM,
                NETEM_DELAY, latency_str.c_str(), NETEM_LOSS, packet_loss_str.c_str(), NETEM_LIMIT, NETEM_LIMIT_VALUE,
                nullptr};
        res = network_emulator::commons::SysHelper::systemCall((char **) args, nullptr);
    }

    return res;
}

int Netem::removeLatencyAndPacketLoss(std::string *iface, bool root) {

    if (TRACE) {
        std::cout << "Netem: removing latency and packet loss of " << *iface << std::endl;
    }

    int res = 0;
    if (root) {
        char const *args[] = {NETEM_TC, NETEM_QDISC, NETEM_DEL, NETEM_DEV, iface->c_str(), NETEM_ROOT, NETEM, nullptr};
        res = network_emulator::commons::SysHelper::systemCall((char **) args, nullptr);
    } else {
        char const *args[] =
            {NETEM_TC, NETEM_QDISC, NETEM_DEL, NETEM_DEV, iface->c_str(), NETEM_PARENT, NETEM_RULE_1_12, NETEM,
                nullptr};
        res = network_emulator::commons::SysHelper::systemCall((char **) args, nullptr);
    }

    return res;
}

int Netem::addLatency(std::string *iface, network_emulator::commons::Rules *new_rule,
    network_emulator::commons::Rules *existing_rule, bool root) {

    std::string latency_str = std::to_string(new_rule->getLatency()) + "ms";
    std::string mode;
    int res = 0;

    if (existing_rule == nullptr || (existing_rule->getLatency() <= 0 && existing_rule->getPacketLoss() <= 0)) {
        mode = NETEM_ADD;
        if (TRACE) {
            std::cout << "Netem: Adding latency of " << latency_str << " to " << *iface << std::endl;
        }
    } else {
        mode = NETEM_CHANGE;
        if (TRACE) {
            std::cout << "Netem: Changing latency to " << latency_str << " to " << *iface << std::endl;
        }
    }

    if (root) {
        char const *args[] =
            {NETEM_TC, NETEM_QDISC, mode.c_str(), NETEM_DEV, iface->c_str(), NETEM_ROOT, NETEM, NETEM_DELAY,
                latency_str.c_str(), NETEM_LIMIT, NETEM_LIMIT_VALUE, nullptr};
        res = network_emulator::commons::SysHelper::systemCall((char **) args, nullptr);
    } else {
        char const *args[] =
            {NETEM_TC, NETEM_QDISC, mode.c_str(), NETEM_DEV, iface->c_str(), NETEM_PARENT, NETEM_RULE_1_12, NETEM,
                NETEM_DELAY, latency_str.c_str(), NETEM_LIMIT, NETEM_LIMIT_VALUE, nullptr};
        res = network_emulator::commons::SysHelper::systemCall((char **) args, nullptr);
    }

    return res;
}

int Netem::addPacketLoss(std::string *iface, network_emulator::commons::Rules *new_rule,
    network_emulator::commons::Rules *existing_rule, bool root) {

    std::string packet_loss_str = std::to_string(new_rule->getPacketLoss()) + "%";
    std::string mode;
    int res = 0;

    if (existing_rule == nullptr || (existing_rule->getLatency() <= 0 && existing_rule->getPacketLoss() <= 0)) {
        mode = NETEM_ADD;
        if (TRACE) {
            std::cout << "Netem: Adding packet loss of " << packet_loss_str << " to " << *iface << std::endl;
        }
    } else {
        mode = NETEM_CHANGE;
        if (TRACE) {
            std::cout << "Netem: Changing packet loss to " << packet_loss_str << " to " << *iface << std::endl;
        }
    }

    if (root) {
        char const *args[] =
            {NETEM_TC, NETEM_QDISC, mode.c_str(), NETEM_DEV, iface->c_str(), NETEM_ROOT, NETEM, NETEM_LOSS,
                packet_loss_str.c_str(), nullptr};
        res = network_emulator::commons::SysHelper::systemCall((char **) args, nullptr);
    } else {
        char const *args[] =
            {NETEM_TC, NETEM_QDISC, mode.c_str(), NETEM_DEV, iface->c_str(), NETEM_PARENT, NETEM_RULE_1_12, NETEM,
                NETEM_LOSS, packet_loss_str.c_str(), nullptr};
        res = network_emulator::commons::SysHelper::systemCall((char **) args, nullptr);
    }
    return res;
}
}
}

