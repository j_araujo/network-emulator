/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

#include <iostream>
#include <sstream>
#include "Parser.h"
#include <unordered_map>
#include <algorithm>

namespace network_emulator {
namespace netem {


#define SPACE ' '
#define NETEM_IFB "ifb"
#define NETEM_INGRESS "ingress"
#define NETEM_QDISC "qdisc"
#define NETEM_DEV "dev"


std::vector<std::string> Parser::getAvailableInterfaces(std::string tc_output) {

    std::vector<std::string> results;

    std::stringstream lines_ss(tc_output);
    while (lines_ss.good()) {
        std::string line;
        getline(lines_ss, line, '\n');

        if (line.empty()) {
            continue;
        }

        auto line_ss = new std::stringstream(line);
        while (line_ss->good()) {
            std::string value;
            getline(*line_ss, value, SPACE);
            if (line.empty()) {
                continue;
            }

            if (value == NETEM_QDISC) {
                getline(*line_ss, value, SPACE);
                if (value == NETEM_INGRESS) {
                    //ignoring ingress
                    continue;
                }
            }

            if (value == NETEM_DEV) {
                getline(*line_ss, value, SPACE);

                if (!value.empty()) {
                    std::find(results.begin(), results.end(), value);
                    if (std::find(results.begin(), results.end(), value) == results.end()) {
                        std::string prefix(NETEM_IFB);
                        if (value.compare(0, prefix.size(), prefix) != 0) {
                            results.emplace_back(value);
                        }
                    }
                    continue;
                }
            }
        }
    }

    return results;
}
}
}