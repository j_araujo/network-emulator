/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

#pragma once

#include <vector>
#include <string>
#include <unordered_map>
#include <Interface.h>

namespace network_emulator {
namespace netem {

/**
 * Creates and executes all the necessary commands to emulate network conditions such as packet loss, latency and
 * rate limit
 */

class Netem {
  public:
    Netem() {

        _rules_per_iface = new std::unordered_map<std::string, network_emulator::commons::Interface>();
    };

    /**
     * Calls the tc command to list all the available interfaces.
     * Virtual interfaces (ifb*) are ignored.
     *
     * @return an array with all the available interfaces' names
     */
    std::vector<std::string> getNetworkInterfaces();

    /**
     * Returns all configured rules for a specific interface
     *
     * @param iface interface name
     * @return rules of a interface
     */
    network_emulator::commons::Interface getInterfaceRules(std::string iface);

    /**
     * Starts the network emulator.
     * NOTE: this will erase all the currently configured tc rules applied on the machine
     * @return status of the operation (0 if successfull)
     */
    int start();

    /**
     * Stops the network emulator.
     * NOTE: this will erase all the currently configured tc rules applied on the machine
     * @return status of the operation (0 if successfull)
     */
    int stop();

    /**
     * Applies a new network condition rule on a interface
     *
     * @param iface interface to where the rule should be applied
     * @param rule the network rule to be applied
     * @param incoming if the rule should be applied to incoming or outgoing traffic
     * @return status of the operation (0 if successfull)
     */
    int addRule(std::string iface, network_emulator::commons::Rules *rule, bool incoming);

    /**
     * Removes all configured rules on a network interface
     *
     * @param network_interface the name of the network interface
     * @return status of the operation (0 if successfull)
     */
    int removeRulesForInterface(std::string network_interface);

    /**
     * Removes all rules for a network interface and deletes the virtual interface associated
     *
     * @param network_interface the name of the network interface
     * @return status of the operation (0 if successfull)
     */
    int removeInterface(std::string network_interface);

  private:

    /**
     * Starts mod probe, used to create virtual interfaces
     * @param n_interfaces  number of interfaces to be creates
     * @return status of the operation (0 if successfull)
     */
    int startModProbe(int n_interfaces);

    /**
     * stops modprobe
     * @return status of the operation (0 if successfull)
     */
    int stopModProbe();

    /**
     * Creates a new virtual interface associated with a interface
     * @param real_iface the name of the real interface to be associated with a new virtual interface
     * @return status of the operation (0 if successfull)
     */
    std::string addVirtualIface(std::string real_iface);

    /**
     * Adds a new rate limit on a specified interface
     * @param iface the interface where the rule will be applied
     * @param new_rule the rule to be applied
     * @param existing_rule the current rule on that interface
     * @return status of the operation (0 if successfull)
     */
    int addRateLimit(std::string *iface, network_emulator::commons::Rules *new_rule,
        network_emulator::commons::Rules *existing_rule);

    /**
     * Adds latency and packet loss to a specified interface
     * @param iface the interface where the rule will be applied
     * @param new_rule the rule to be applied
     * @param existing_rule the current rule on that interface
     * @param root if is a root rule or a if one already exists
     * @return status of the operation (0 if successfull)
     */
    int addLatencyAndPacketLoss(std::string *iface, network_emulator::commons::Rules *new_rule,
        network_emulator::commons::Rules *existing_rule, bool root);

    /**
     * @brief Removes latency and packet loss on a specified interface
     * @param iface the interface where the rule will be applied
     * @param root if is a root rule or a if one already exists
     * @return status of the operation (0 if successfull)
     */
    int removeLatencyAndPacketLoss(std::string *iface, bool root);

    /**
     * Adds packet loss to a specified interface
     * @param iface the interface where the rule will be applied
     * @param new_rule the rule to be applied
     * @param existing_rule the current rule on that interface
     * @param root if is a root rule or a if one already exists
     * @return status of the operation (0 if successfull)
     */
    int addLatency(std::string *iface, network_emulator::commons::Rules *new_rule,
        network_emulator::commons::Rules *existing_rule, bool root);

    /**
     * Adds latency to a specified interface
     * @param iface the interface where the rule will be applied
     * @param new_rule the rule to be applied
     * @param existing_rule the current rule on that interface
     * @param root if is a root rule or a if one already exists
     * @return status of the operation (0 if successfull)
     */
    int addPacketLoss(std::string *iface, network_emulator::commons::Rules *new_rule,
        network_emulator::commons::Rules *existing_rule, bool root);

    /**
     * Rules applied per each interface
     */
    std::unordered_map<std::string, network_emulator::commons::Interface> *_rules_per_iface;
};
}
}