
#include <gtest/gtest.h>
#include <chrono>
#include <thread>
#include "../../include/Rules.h"
#include "../../include/Interface.h"

namespace {

TEST(Interface, Constructor) {
	std::string iface = "iface";
	network_emulator::commons::Interface interface(iface);

	ASSERT_EQ(iface, interface.getPhysicalIface());
	ASSERT_EQ(interface.getIncomingRules(), nullptr);
	ASSERT_EQ(interface.getOutgoingRules(), nullptr);
}

TEST(Interface, GettersAndSetters) {
	std::string iface = "iface";
	network_emulator::commons::Interface interface(iface);

	auto inRules = new network_emulator::commons::Rules(0, 0, 0);
	auto outRules = new network_emulator::commons::Rules(1, 1, 1);

	interface.setIncomingRules(inRules);
	interface.setOutgoingRules(outRules);

	ASSERT_EQ(interface.getIncomingRules()->getRate(), inRules->getRate());
	ASSERT_EQ(interface.getIncomingRules()->getPacketLoss(), inRules->getPacketLoss());
	ASSERT_EQ(interface.getIncomingRules()->getLatency(), inRules->getLatency());

	ASSERT_EQ(interface.getOutgoingRules()->getRate(), outRules->getRate());
	ASSERT_EQ(interface.getOutgoingRules()->getPacketLoss(), outRules->getPacketLoss());
	ASSERT_EQ(interface.getOutgoingRules()->getLatency(), outRules->getLatency());
}

TEST(Interface , JSON) {
	std::string iface = "iface";
	network_emulator::commons::Interface interface(iface);

	auto inRules = new network_emulator::commons::Rules(0, 0, 0);
	auto outRules = new network_emulator::commons::Rules(1, 1, 1);

	interface.setIncomingRules(inRules);
	interface.setOutgoingRules(outRules);

	nlohmann::json json = interface.toJson();

	network_emulator::commons::Interface interface_from_json(json);
	ASSERT_EQ(interface_from_json.getPhysicalIface(), iface);

	ASSERT_EQ(interface_from_json.getIncomingRules()->getRate(), inRules->getRate());
	ASSERT_EQ(interface_from_json.getIncomingRules()->getPacketLoss(), inRules->getPacketLoss());
	ASSERT_EQ(interface_from_json.getIncomingRules()->getLatency(), inRules->getLatency());

	ASSERT_EQ(interface_from_json.getOutgoingRules()->getRate(), outRules->getRate());
	ASSERT_EQ(interface_from_json.getOutgoingRules()->getPacketLoss(), outRules->getPacketLoss());
	ASSERT_EQ(interface_from_json.getOutgoingRules()->getLatency(), outRules->getLatency());
}

}
