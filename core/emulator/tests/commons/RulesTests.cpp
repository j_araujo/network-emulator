
#include <gtest/gtest.h>
#include <chrono>
#include <thread>
#include "../../include/Rules.h"

namespace {

TEST(Rules, Constructor) {
	std::string iface = "iface";
	int latency = 10, rate = 100;
	float packet_loss = 0.1;
	network_emulator::commons::Rules rules(packet_loss, latency, rate);

	ASSERT_EQ(packet_loss, rules.getPacketLoss());
	ASSERT_EQ(latency, rules.getLatency());
	ASSERT_EQ(rate, rules.getRate());

}

TEST(Rules, GettersAndSetters) {
	std::string iface = "iface";
	int latency = 10, rate = 100;
	float packet_loss = 0.1;

	int new_latency = 200, new_rate = 32000;
	float new_packet_loss = 0.75;

	network_emulator::commons::Rules rules(packet_loss, latency, rate);

	rules.setPacketLoss(new_packet_loss);
	rules.setLatency(new_latency);
	rules.setRate(new_rate);

	ASSERT_EQ(new_packet_loss, rules.getPacketLoss());
	ASSERT_EQ(new_latency, rules.getLatency());
	ASSERT_EQ(new_rate, rules.getRate());
}

TEST(Rules, JSON) {
	std::string iface = "iface";
	int latency = 10, rate = 100;
	float packet_loss = 0.1;
	network_emulator::commons::Rules rules(packet_loss, latency, rate);

	nlohmann::json json = rules.toJson();


	network_emulator::commons::Rules rules_from_json(json);
	ASSERT_EQ(packet_loss, rules_from_json.getPacketLoss());
	ASSERT_EQ(latency, rules_from_json.getLatency());
	ASSERT_EQ(rate, rules_from_json.getRate());
}

}


