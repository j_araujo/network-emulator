#include <gtest/gtest.h>
#include <chrono>
#include <thread>
#include "../../../src/emulators/netem/Parser.h"

namespace {

TEST(Parser, GetAvailableIfaces) {
	std::string
		tc_output = "qdisc pfifo_fast 0: dev eth0 root refcnt 2 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1\n";
	network_emulator::netem::Parser parser;
	auto ifaces = parser.getAvailableInterfaces(tc_output);
	ASSERT_EQ(1, ifaces.size());
	ASSERT_EQ(ifaces[0], "eth0");
}

TEST(Parser, GetMultipleAvailableIfaces) {
	std::string
		tc_output = "qdisc pfifo_fast 0: dev eth0 root refcnt 2 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1\n"
					"qdisc htb 1: dev eth1 root refcnt 2 r2q 10 default 12 direct_packets_stat 0 direct_qlen 1000\n"
					"qdisc netem 8010: dev eth1 parent 1:12 limit 1000 loss 1%\n"
					"qdisc ingress ffff: dev eth1 parent ffff:fff1 ----------------\n"
					"qdisc pfifo_fast 0: dev ifb0 root refcnt 2 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1\n"
					"qdisc pfifo_fast 0: dev ifb1 root refcnt 2 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1\n"
					"qdisc pfifo_fast 0: dev faksfni312o3i1o2 root refcnt 2 bands 3 priomap  1 2 2 2 1 2 0 0 1 1 1 1 1 1 1 1\n";
	network_emulator::netem::Parser parser;
	auto ifaces = parser.getAvailableInterfaces(tc_output);
	ASSERT_EQ(3, ifaces.size());
	ASSERT_EQ(ifaces[0], "eth0");
	ASSERT_EQ(ifaces[1], "eth1");
	ASSERT_EQ(ifaces[2], "faksfni312o3i1o2");
}
}