/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

#pragma once

#include <vector>
#include <string>
#include "json.hpp"

namespace network_emulator {
namespace commons {

/**
 * This class represents a set of network condition rules. Each rule must contain the interface that the rule refers
 * to, and optional values for packet loss, latency and rate limit.
 */
class Rules {
  public:

    /**
     * Default constructor
     */
    Rules() = default;

    /**
     * Creates a new set of rules
     *
     * @param packet_loss percentage of packets to be dropped
     * @param latency latency to be applied, in ms
     * @param rate limit of bandwith, in Mbit/s
     */
    Rules(float packet_loss, int latency, int rate) : _packet_loss(packet_loss), _latency(latency), _rate(rate) {
    };

    /**
     * Creates a new set of rules from a JSON
     *
     * @param input_json json that contains the network conditions to be applied
     */
    Rules(nlohmann::json input_json);

    /**
     * Generates a JSON representation of this Rules
     *
     * @return a json object with the rules
     */
    nlohmann::json toJson();

    float getPacketLoss() { return _packet_loss; };

    void setPacketLoss(float packet_loss) { _packet_loss = packet_loss; };

    int getLatency() { return _latency; };

    void setLatency(int latency) { _latency = latency; };

    int getRate() { return _rate; };

    void setRate(int rate) { _rate = rate; };

  private:
    /**
     * Packet loss percentage
     */
    float _packet_loss{0};

    /**
     * Latency, in ms
     */
    int _latency{0};

    /**
     * Rate limit, in Mbits/s
     */
    int _rate{0};
};
}
}