/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

#pragma once

#include <vector>
#include <string>
#include "Rules.h"
#include "json.hpp"

namespace network_emulator {
namespace commons {

/**
 * This class represents a network condition profile. Each profile is defined by a name and rules for incoming and
 * outgoing traffic
 */
class Profile {
  public:

    /**
     * Default constructor
     */
    Profile() = default;

    /**
     * Creates a profile with a name
     * @param name
     */
    explicit Profile(std::string name) : _name(name) {};

    /**
     * Loads all the profiles available in a file path
     *
     * @param folder the path of the folder where the profiles are stored
     * @return profiles available in the folder
     */
    static std::vector<Profile> loadProfiles(std::string folder);

    /**
     * Writes a profile (as json) into a specific path
     *
     * @param path the path of the folder where the profiles are stored
     */
    void saveToFile(std::string folder);

    /**
     * Generates a JSON representation of this Profile
     *
     * @return a json object with the rules for this profile
     */
    nlohmann::json toJson();

    /**
     * Creates a profile from a json structure
     *
     * @param json JSON representation of a profile
     * @return a profile object
     */
    static Profile loadFromJSON(nlohmann::json json);

    std::string getName() { return _name; };

    network_emulator::commons::Rules *getIncomingRules() { return _incoming_rules; };

    void setIncomingRules(network_emulator::commons::Rules *incomingRules) { _incoming_rules = incomingRules; };

    network_emulator::commons::Rules *getOutgoingRules() { return _outgoing_rules; };

    void setOutgoingRules(network_emulator::commons::Rules *outGoingRules) { _outgoing_rules = outGoingRules; };

  private:

    /**
     * Packet loss percentage
     */
    std::string _name{""};

    /**
     * Rules for the incoming traffic
     */
    Rules *_incoming_rules{nullptr};

    /**
     * Rules for the outgoing traffic
     */
    Rules *_outgoing_rules{nullptr};
};
}
}