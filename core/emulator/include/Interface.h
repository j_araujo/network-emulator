/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

#pragma once

#include <vector>
#include <string>
#include <Rules.h>

namespace network_emulator {
namespace commons {

/**
 * An interface is composed by the pyshical interface name, and network rules for the incoming and outgoing traffic.
 * These rules are on separated interfaces, as one is applied on the physical interface and the other is applied on
 * a virtual interface created just to apply this rules.
 */
class Interface {

  public:
    /**
     * @brief Default constructor
     */
    Interface() = default;

    /**
     * Constructor with the pyshical interface that will identify this interface
     * @param physical_iface interface's identifier
     */
    explicit Interface(std::string physical_iface) : _physical_iface(physical_iface) {
    };

    /**
     * Creates a new Interface definition from a JSON object
     *
     * @param input_json json that contains the network conditions to be applied to a interface
     */
    explicit Interface(nlohmann::json input_json);

    /**
     * Generates a JSON representation of this Interface and its network rules
     *
     * @return a json object with the rules for this interface
     */
    nlohmann::json toJson();

    /**
     * Parses a JSON into a new Interface object
     * @param json_path the path of the json to be loaded
     * @return a new Interface
     */
    static Interface loadJson(std::string json_path);

    std::string getPhysicalIface() { return _physical_iface; };

    std::string getVirtualIface() { return _virtual_iface; };

    void setVirtualIface(std::string virtual_iface) { _virtual_iface = virtual_iface; };

    Rules *getIncomingRules() { return _incoming_rules; };

    void setIncomingRules(Rules *incomingRules) { _incoming_rules = incomingRules; };

    Rules *getOutgoingRules() { return _outgoing_rules; };

    void setOutgoingRules(Rules *outGoingRules) { _outgoing_rules = outGoingRules; };

  private:
    /**
     * The physical interface
     */
    std::string _physical_iface{""};

    /**
     * The virtual interface, if required
     */
    std::string _virtual_iface{""};

    /**
     * Currently configured rules for the incoming traffic
     */
    Rules *_incoming_rules{nullptr};

    /**
     * Currently configured rules for the outgoing traffic
     */
    Rules *_outgoing_rules{nullptr};
};
}
}