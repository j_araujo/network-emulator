/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

#pragma once

#include <sys/socket.h>
#include <sys/un.h>
#include <sys/stat.h>
#include <string>
#include <Profile.h>

/**
 * Possible options
 */

/**
 * IPC sock
 */
#define ARG_SOCK "-sock"

/**
 * Interface where the rules will be applied
 */
#define ARG_DEV "-dev"

/**
 * Packet loss, in %
 */
#define ARG_PLR "-plr"

/**
 * Latency, in ms
 */
#define ARG_LATENCY "-latency"

/**
 * Rate limit, in Mbps
 */
#define ARG_RATE "-rate"

/**
 * Flow to where the rules should be applied
 */
#define ARG_FLOW "-flow"
#define ARG_FLOW_INCOMING "incoming"
#define ARG_FLOW_OUTGOING "outgoing"

/**
 * Possible options
 */

/**
 * Show current rules
 */
#define ARG_SHOW "show"

/**
 * Adds a new rule
 */
#define ARG_ADD "add"

/**
 * Loads a new profile
 */
#define ARG_LOAD "load"

/**
 * Deletes a rule
 */
#define ARG_DELETE "delete"

#define JSON_COMMAND "COMMAND"
#define JSON_RULES "RULES"
#define JSON_IFACE "INTERFACE"
#define JSON_INTERFACES "INTERFACES"
#define JSON_STATUS "STATUS"
#define JSON_OK "OK"
#define JSON_INCOMING "INCOMING"
#define JSON_OUTGOING "OUTGOING"
#define JSON_PROFILES "PROFILES"
#define JSON_PROFILE "PROFILE"
#define JSON_NAME "NAME"
#define JSON_PACKET_LOSS "PACKET_LOSS"
#define JSON_LATENCY "LATENCY"
#define JSON_RATE_LIMIT "RATE_LIMIT"

namespace network_emulator {

/**
 * @brief default IPC sock, to communicate with the emulator daemon
 */
#define DEFAULT_IPC_SOCK "/tmp/cvl_netem.sock"

/**
 * @brief default path for storing the profiles
 */
#define DEFAULT_PROFILES_PATH "profiles/"

#define DEFAULT_ERROR_MESSAGE "{\"STATUS\": \"ERROR\"}"
#define DEFAULT_OK_MESSAGE "{\"STATUS\": \"OK\"}"

/**
 * Messages that can be processed by emulator service
 */
enum MessageTypes {
    GET_RULES = 0, ADD_RULE, DELETE_RULES, GET_PROFILES, SAVE_PROFILE, SHUTDOWN
};

/**
 * Service that waits for input messages and redirects them to the proper network emulator (e.g, netem)
 */
class Emulator {
  public:
    /**
     * Starts an IPC socket and waits for incoming requests;
     *
     * @param run_background if true, the daemon will run in background mode
     * @param sock_address the address where the service will listen to messages
     * @param profiles_folder the path where the profiles will be loaded from/stored into
     * @return status code (0 for success, negative otherwise)
     */
    int startEmulator(bool run_background, const char *sock_address, const char *profiles_folder);

    /**
     * Starts an IPC socket and waits for incoming requests
     *
     * @param run_background if true, the daemon will run in background mode
     * @param sock_address the address where the service will listen to messages
     * @param profiles_folder the path where the profiles will be loaded from/stored into
     * @param incoming_interface if configured, the service will define this interface as the default for input rules,
     * and will ignore other interfaces
     * @param outgoing_interface if configured, the service will define this interface as the default for output rules,
     * and will ignore other interfaces
     * @return status code (0 for success, negative otherwise)
     */
    int startEmulator(bool run_background, const char *sock_address, const char *profiles_folder,
        const char *incoming_interface, const char *outgoing_interface);

    /**
     * Sends a message to the network emulator, waiting for a reply
     *
     * @param address the ipc path where the emulator is listening for messages
     * @param message the message to be sent, in a json format
     * @return a string with the emulator's reply
     */
    static std::string sendMessageToEmulator(const char *address, const char *message);

  private:
    /**
     * Processes a new message received on the IPC socket
     *
     * @param message JSON with the message
     * @param client_socket_fd client file descriptor to reply to
     * @return status code (0 for success, negative otherwise)
     */
    int processMessage(char *message, int client_socket_fd);

    /**
     * fd for communicating with ipc
     */
    int _sock_fd;

    /**
     * List of stored profiles
     */
    std::vector<network_emulator::commons::Profile> _profiles;

    /**
     * If the service is currently running
     */
    bool _is_running;

    /**
     * if configured, the service will define this interface as the default for input rules,
     * and will ignore other interfaces
     */
    std::string _incoming_interface{""};

    /**
     * if configured, the service will define this interface as the default for output rules,
     * and will ignore other interfaces
     */
    std::string _outgoing_interface{""};
};
}