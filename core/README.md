# Codavel's Network Emulator

## Emulator core

The core of Codavel's Network Emulator is a c++ library that interacts with Linux's netem module to provide
a way of applying different network conditions to a network.

On start, the tool lists the available interfaces, clears any existing netem rules, and creates an IPC sock to listen
for incoming requests.

Currently, the Network Emulator allows the configuration of:
* Bandwidth Limit
* Latency
* Packet Loss

The Rules are applied by creating a Hierarchical Token Bucket (HTB) root handle for the interface and by adding a htb class for the rate limiting and using netem for the latency and packet loss.

Given that netem by default only applies rules to outgoing traffic, to apply rules also on the incoming traffic, the emulator creates one virtual interface using the Intermediate Functional Block (IFB) and redirects the incoming
traffic to that virtual interface, where network conditions could be applied as in the physical interface.


## Example

To apply a rate limit of 1000Bkit, 0.1% of packet loss and 200ms of latency, the Emulator performs the following commands:

 ``` bash
tc qdisc add dev eth0 root handle 1: htb default 12
tc class add dev eth0 parent 1:1 classid 1:12 htb rate 1Mbit ceil 1000Kbit
tc qdisc add dev eth0 parent 1:12 netem loss 0.1% delay 200ms
 ```  


To add a new virtual interface to be able to apply rules on the incoming traffic, the Emulator performs the following commands:

 ``` bash
modprobe ifb numifbs=1
ip link set dev ifb0 up
 ```  

And, to redirect all the incoming traffic to the newly created virtual interface ifb0, performs:

 ``` bash
tc qdisc add dev eth0 handle ffff: ingress
tc filter add dev eth0 parent ffff: protocol ip u32 match u32 0 0 action mirred egress redirect dev ifb0
 ```  
 
After this steps, rules can be applied either in the outgoing traffic (using the interface eth0) or in the incoming traffic (using the virtual interface ifb0)