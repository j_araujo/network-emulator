/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

#include <vector>
#include <string.h>
#include <iostream>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>
#include <unistd.h>
#include <poll.h>
#include <Emulator.h>
#include <Rules.h>
#include <Interface.h>


/**
 * @brief Starts the emulator daemon process, and waits for message on the IPC
 * @param argc number of arguments
 * @param argv arguments, which can include --incoming for configuring a incoming interface, and --outgoing for
 * configuring a outgoing interface
 * @return exit code
 */
int main(int argc, char *argv[]) {

    char *incoming_interface = nullptr;
    char *outgoing_interface = nullptr;

    for (int i = 1; i < argc; i++) {
        if (strcmp(argv[i], ARG_FLOW_INCOMING) == 0) {
            incoming_interface = argv[++i];
            continue;
        }

        if (strcmp(argv[i], ARG_FLOW_OUTGOING) == 0) {
            outgoing_interface = argv[++i];
            continue;
        }
    }

    network_emulator::Emulator emulator;
    emulator.startEmulator(false, nullptr, nullptr, incoming_interface, outgoing_interface);
}