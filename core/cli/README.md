# Codavel's Network Emulator

## Emulator CLI

Emulator Command Line Interface (CLI) is a tool to communicate with the Emulator core service via a terminal.
 

## Bulding

To build the CLI executable, access the root folder of the network emulator project and run:

``` bash
cmake . && make && make install
```

This will compile the code and copy the executable to _build/bin/network_emulator_cli_.

## Running

The CLI provides the following operation modes:
* `show`: Shows the current network conditions for all the available interfaces
   
   To have access to the currently configured network conditions, run:
   
 ``` bash
./emulator_cli show
 ```  
   
* `add`: Adds a new network condition rule for a specific interface. 

   To add a new network rule, you must provide the interface (-dev <iface>) and at least one of the available options:
   
   * Packet loss rate: `-plr <percentage>`
   * Latency: `-latency <ms>`
   * Bandwidth Limit: `-rate <Kbits/s>`
   * Traffic direction: `-flow <"Incoming" or "Outgoing">`

   For example, to add 100ms of latency to both incoming and outgoing traffic on the interface eth0, run the following command:

 ``` bash
./emulator_cli add -dev eth0 -latency 100
 ```  
 

* `delete`: Deletes the network conditions from all interfaces or from a specific one
    
    To delete all rules previously applied to a specific interface, run:
    
 ``` bash
./emulator_cli del -dev <interface>
 ```  

* `load`: Loads network conditions from a specific JSON file

    To load and apply rules stored in a JSON file, run:
    
 ``` bash
./emulator_cli load profiles/profile.json
 ```  