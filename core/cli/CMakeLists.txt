cmake_minimum_required(VERSION 3.9)

set(CMAKE_EXPORT_COMPILE_COMMANDS 1)

######################################
########## COMMONS LIBRARY ###########
######################################

file(GLOB_RECURSE INCS "${CMAKE_CURRENT_SOURCE_DIR}/include/*.h")
file(GLOB_RECURSE SRCS "${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp" "${CMAKE_CURRENT_SOURCE_DIR}/src/*.h")
list(REMOVE_ITEM SRCS "${CMAKE_CURRENT_SOURCE_DIR}/src/EmulatorCli.cpp")

add_executable(network_emulator_cli ${CMAKE_CURRENT_SOURCE_DIR}/src/EmulatorCli.cpp ${SRCS} ${INCS})

target_link_libraries(network_emulator_cli ${EMULATOR_LIBRARIES})

# add the include dir as a public include directory
# will be inherited by dependent targets
# add the include directory src which will only be visible by
# sample_exe
target_include_directories(network_emulator_cli
        PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include
        PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/src)


set_target_properties(network_emulator_cli
        PROPERTIES
        RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/build"
        )


INSTALL(TARGETS network_emulator_cli
        RUNTIME DESTINATION "${CMAKE_SOURCE_DIR}/build/bin"
        )