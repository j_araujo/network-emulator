/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

#include <vector>
#include <string.h>
#include <iostream>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>
#include <unistd.h>
#include <poll.h>
#include <Emulator.h>
#include <Rules.h>
#include <Interface.h>

/**
 * Parses arguments from command line and sends the parsed rule to emulator
 *
 * @param argc number of arguments
 * @param argv arguments
 * @return status of the operation
 */
static int addRuleFromArgs(int argc, char *argv[]) {

    std::string iface, sock;
    int latency = -1, rate = -1, mode = 0;
    float packet_loss = -1;

    for (int i = 2; i < argc; i++) {
        if (strcmp(argv[i], ARG_SOCK) == 0) {
            sock = argv[++i];
            continue;
        }

        if (strcmp(argv[i], ARG_DEV) == 0) {
            iface = argv[++i];
            continue;
        }

        if (strcmp(argv[i], ARG_PLR) == 0) {
            packet_loss = (float) std::stod(argv[++i]);
            continue;
        }

        if (strcmp(argv[i], ARG_LATENCY) == 0) {
            latency = std::stoi(argv[++i]);
            continue;
        }

        if (strcmp(argv[i], ARG_RATE) == 0) {
            rate = std::stoi(argv[++i]);
            continue;
        }

        if (strcmp(argv[i], ARG_FLOW) == 0) {
            std::string flow = std::string(argv[++i]);

            if (flow == ARG_FLOW_INCOMING) {
                mode = 1;
                continue;
            }

            if (flow == ARG_FLOW_OUTGOING) {
                mode = 2;
                continue;
            }

            std::cout << "Unknown flow! Available: " << ARG_FLOW_INCOMING << " or " << ARG_FLOW_OUTGOING << std::endl;
            return -1;
        }

        std::cout << "Unknown argument " << argv[i] << std::endl;
        return -1;
    }

    if (!iface.empty()) {
        auto rule = new network_emulator::commons::Rules(packet_loss, latency, rate);
        network_emulator::commons::Interface interface(iface);
        nlohmann::json json;

        switch (mode) {
            case 0: {
                interface.setIncomingRules(rule);
                interface.setOutgoingRules(rule);
                break;
            }
            case 1: {
                interface.setIncomingRules(rule);
                break;
            }
            case 2: {
                interface.setOutgoingRules(rule);
                break;
            }

            default: break;
        }

        std::string ipc_sock = (sock.empty() ? DEFAULT_IPC_SOCK : sock);

        json[JSON_COMMAND] = network_emulator::ADD_RULE;
        json[JSON_RULES] = interface.toJson();
        network_emulator::Emulator::sendMessageToEmulator(ipc_sock.c_str(), json.dump().c_str());

        return 0;
    }

    return -1;
}

/**
 * @brief Parses the arguments received and creates a message to be sent to the network emulator daemon
 * @param argc number of arguments
 * @param argv arguments
 * @return exit code
 */
int main(int argc, char *argv[]) {

    std::string sock;
    nlohmann::json json;

    if (argc > 1) {
        if (strcmp(argv[1], ARG_SHOW) == 0) {
            json[JSON_COMMAND] = network_emulator::GET_RULES;

            for (int i = 2; i < argc; i++) {
                if (strcmp(argv[i], ARG_SOCK) == 0) {
                    sock = argv[++i];
                    continue;
                }
            }

            std::string ipc_sock = (sock.empty() ? DEFAULT_IPC_SOCK : sock);
            network_emulator::Emulator::sendMessageToEmulator(ipc_sock.c_str(), json.dump().c_str());
            return 0;
        } else if (strcmp(argv[1], ARG_ADD) == 0) {
            int res = addRuleFromArgs(argc, argv);
            if (res == 0) {
                return res;
            }
        } else if (strcmp(argv[1], ARG_LOAD) == 0) {
            std::string json_path(argv[2]);

            auto interface = network_emulator::commons::Interface::loadJson(json_path);
            if (interface.getPhysicalIface().empty()) {
                return -1;
            }

            for (int i = 2; i < argc; i++) {
                if (strcmp(argv[i], ARG_SOCK) == 0) {
                    sock = argv[++i];
                    continue;
                }
            }
            std::string ipc_sock = (sock.empty() ? DEFAULT_IPC_SOCK : sock);

            json[JSON_COMMAND] = network_emulator::ADD_RULE;
            json[JSON_RULES] = interface.toJson();

            network_emulator::Emulator::sendMessageToEmulator(ipc_sock.c_str(), json.dump().c_str());
            return 0;
        } else if (strcmp(argv[1], ARG_DELETE) == 0) {
            std::string iface;

            for (int i = 2; i < argc; i++) {
                if (strcmp(argv[i], ARG_DEV) == 0) {
                    iface = argv[++i];
                }
            }

            if (!iface.empty()) {
                std::string ipc_sock = (sock.empty() ? DEFAULT_IPC_SOCK : sock);

                json[JSON_COMMAND] = network_emulator::DELETE_RULES;
                json[JSON_IFACE] = iface;

                network_emulator::Emulator::sendMessageToEmulator(ipc_sock.c_str(), json.dump().c_str());
                return 0;
            }
        }
    }

    std::cout << "Wrong usage!" << std::endl;

    std::cout << "Usage: network_emulator MODE [OPTIONS]" << std::endl;
    std::cout << "\twhere\tMODE := { " << ARG_SHOW << " | " << ARG_ADD << " | " << ARG_DELETE << " | " << ARG_LOAD
              << " }" << std::endl;
    std::cout << "\t\tOPTIONS := { " << ARG_SOCK << " | " << ARG_DEV << " | " << ARG_PLR << " | " << ARG_LATENCY
              << " | " << ARG_RATE << " | " << ARG_FLOW << " { " << ARG_FLOW_INCOMING << " | " << ARG_FLOW_OUTGOING
              << "} }" << std::endl;
}