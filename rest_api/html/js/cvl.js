var profiles_json;

function sendNewRules(i) {
    var incoming_plr = parseFloat($('#incoming_plr_' + i).val());
    var outgoing_plr = parseFloat($('#outgoing_plr_' + i).val());
    var incoming_latency = parseInt($('#incoming_latency_' + i).val());
    var outgoing_latency = parseInt($('#outgoing_latency_' + i).val());
    var incoming_rate = parseInt($('#incoming_rate_' + i).val());
    var outgoing_rate = parseInt($('#outgoing_rate_' + i).val());


    var data = JSON.stringify({
        "INTERFACE": i,
        "INCOMING": {"LATENCY": incoming_latency, "PACKET_LOSS": incoming_plr, "RATE_LIMIT": incoming_rate},
        "OUTGOING": {"LATENCY": outgoing_latency, "PACKET_LOSS": outgoing_plr, "RATE_LIMIT": outgoing_rate}
    });

    var xhr = new XMLHttpRequest();
    xhr.open("POST", 'http://' + window.location.host + "/config", true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(data);
    return false;
}

function saveProfiles(iface) {
    var profile_name = $('#profile_name_' + iface + '').val();
    if (!profile_name || profile_name === '') {
        return false;
    }

    var incoming_plr = parseFloat($('#incoming_plr_' + iface).val());
    var outgoing_plr = parseFloat($('#outgoing_plr_' + iface).val());
    var incoming_latency = parseInt($('#incoming_latency_' + iface).val());
    var outgoing_latency = parseInt($('#outgoing_latency_' + iface).val());
    var incoming_rate = parseInt($('#incoming_rate_' + iface).val());
    var outgoing_rate = parseInt($('#outgoing_rate_' + iface).val());

    var data = JSON.stringify({
        "NAME": profile_name,
        "INCOMING": {"LATENCY": incoming_latency, "PACKET_LOSS": incoming_plr, "RATE_LIMIT": incoming_rate},
        "OUTGOING": {"LATENCY": outgoing_latency, "PACKET_LOSS": outgoing_plr, "RATE_LIMIT": outgoing_rate}
    });

    var xhr = new XMLHttpRequest();
    xhr.open("POST", 'http://' + window.location.host + "/profile", true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(data);
    return false;
}


function loadProfiles(iface) {
    var profile_name = $('#profiles_' + iface + ' :selected').text();
    for (var i in profiles_json.PROFILES) {
        if (profiles_json.PROFILES[i].NAME === profile_name) {
            if (profiles_json.PROFILES[i].hasOwnProperty("INCOMING")) {
                if (!$('#incoming_plr_' + iface).prop('disabled') && profiles_json.PROFILES[i].INCOMING.hasOwnProperty("PACKET_LOSS")) {
                    $('#incoming_plr_' + iface).val(profiles_json.PROFILES[i].INCOMING.PACKET_LOSS);
                }

                if (!$('#incoming_latency_' + iface).prop('disabled') && profiles_json.PROFILES[i].INCOMING.hasOwnProperty("LATENCY")) {
                    $('#incoming_latency_' + iface).val(profiles_json.PROFILES[i].INCOMING.LATENCY);
                }

                if (!$('#incoming_rate_' + iface).prop('disabled') && profiles_json.PROFILES[i].INCOMING.hasOwnProperty("RATE_LIMIT")) {
                    $('#incoming_rate_' + iface).val(profiles_json.PROFILES[i].INCOMING.RATE_LIMIT);
                }
            }

            if (profiles_json.PROFILES[i].hasOwnProperty("OUTGOING")) {
                if (profiles_json.PROFILES[i].OUTGOING.hasOwnProperty("PACKET_LOSS")) {
                    $('#outgoing_plr_' + iface).val(profiles_json.PROFILES[i].OUTGOING.PACKET_LOSS);
                }

                if (profiles_json.PROFILES[i].OUTGOING.hasOwnProperty("LATENCY")) {
                    $('#outgoing_latency_' + iface).val(profiles_json.PROFILES[i].OUTGOING.LATENCY);
                }

                if (profiles_json.PROFILES[i].OUTGOING.hasOwnProperty("RATE_LIMIT")) {
                    $('#outgoing_rate_' + iface).val(profiles_json.PROFILES[i].OUTGOING.RATE_LIMIT);
                }
            }

            return false;
        }

    }

    return false;
}

function resetRulesInterfaces(iface) {
    $('#incoming_plr_' + iface).val(0);
    $('#outgoing_plr_' + iface).val(0);
    $('#incoming_latency_' + iface).val(0);
    $('#outgoing_latency_' + iface).val(0);
    $('#incoming_rate_' + iface).val(0);
    $('#outgoing_rate_' + iface).val(0);

    var data = JSON.stringify({
        "INTERFACE": iface,
        "INCOMING": {"LATENCY": 0, "PACKET_LOSS": 0, "RATE_LIMIT": 0},
        "OUTGOING": {"LATENCY": 0, "PACKET_LOSS": 0, "RATE_LIMIT": 0}
    });

    var xhr = new XMLHttpRequest();
    xhr.open("POST", 'http://' + window.location.host + "/config", true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(data);
    return false;
}

function parseInterfaces() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://' + window.location.host + "/config", true);
    xhr.send();

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            var json = JSON.parse(xhr.responseText);
            var profiles = new XMLHttpRequest();
            profiles.open('GET', 'http://' + window.location.host + "/profiles", true);
            profiles.send();

            profiles.onreadystatechange = function () {
                if (profiles.readyState == 4 && profiles.status == 200) {
                    var tabs = '';
                    var content = '';

                    for (var i in json.INTERFACES) {
                        var interface = json.INTERFACES[i].INTERFACE;
                        var index = +i + 1;

                        if (interface !== "DEFAULT") {
                            if (i > 0) {
                                tabs += '<a class="nav-item nav-link" id="nav_iface_' + index + '" data-toggle="tab" href="#iface_' + index + '" role="tab" aria-controls="iface_' + index + '" aria-selected="false">' + interface + ' </a>';
                                content += '<div class="tab-pane fade" id="iface_' + index + '" role="tabpanel"  aria-labelledby="nav_iface_' + index + '">';
                            } else {
                                tabs += '<a class="nav-item nav-link active" id="nav_iface_' + index + '" data-toggle="tab" href="#iface_' + index + '" role="tab" aria-controls="iface_' + index + '" aria-selected="true">' + interface + ' </a>';
                                content += '<div class="tab-pane fade show active" id="iface_' + index + '" role="tabpanel"  aria-labelledby="nav_iface_' + index + '">';


                            }
                            content += '<div id=rules_iface_' + index + '>';
                        }

                        content += '<h1 class="section-subtitle">Rules</h1>'

                        content += '<div class="row">';
                        content += '<div class="output col-sm-2"> </div>';

                        if (json.INTERFACES[i].hasOwnProperty("INCOMING")) {
                            content += '<div class="output column-title col-sm-5"> <p>Incoming </p> </div>';
                        }
                        else {
                            content += '<div class="output column-title col-sm-5"> <p>Incoming Disabled (mising ifb module) </p> </div>';
                        }

                        content += '<div class="output column-title col-sm-5"> <p>Outgoing </p> </div>';
                        content += '</div>';


                        content += '<div class="row">';
                        content += '<div class="output col-sm-2"><p> Latency </p> </div>';
                        content += '<div class="output col-sm-5">';

                        if (json.INTERFACES[i].hasOwnProperty("INCOMING")) {
                            content += '<input type="number" class="form-control" id="incoming_latency_' + interface + '" name="incoming_latency_' + interface + '" value=' + (json.INTERFACES[i].INCOMING.LATENCY) + ' placeholder="Latency (ms)" min="0" step="50" data-bind="value:replyNumber">';
                        } else {
                            content += '<input type="number" class="form-control" id="incoming_latency_' + interface + '" name="incoming_latency_' + interface + '" value=0 disabled>';
                        }

                        content += '</div>';
                        content += '<div class="output col-sm-5">';
                        content += '<input type="number" class="form-control" id="outgoing_latency_' + interface + '" name="outgoing_latency_' + interface + '" value=' + (json.INTERFACES[i].OUTGOING.LATENCY) + ' placeholder="Latency (ms)" min="0" step="50" data-bind="value:replyNumber">';
                        content += '</div>';
                        content += '</div>';


                        content += '<div class="row">';
                        content += '<div class="output col-sm-2"><p> Packet loss rate </p> </div>';
                        content += '<div class="output col-sm-5">';
                        if (json.INTERFACES[i].hasOwnProperty("INCOMING")) {
                            content += '<input type="number" class="form-control" id="incoming_plr_' + interface + '" name="incoming_plr_' + interface + '" value=' + (json.INTERFACES[i].INCOMING.PACKET_LOSS) + ' placeholder="Packet Loss (%)" min="0" step="0.1" data-bind="value:replyNumber">';
                        } else {
                            content += '<input type="number" class="form-control" id="incoming_plr_' + interface + '" name="incoming_plr_' + interface + '" value=0 disabled>';
                        }

                        content += '</div>';
                        content += '<div class="output col-sm-5">';
                        content += '<input type="number" class="form-control" id="outgoing_plr_' + interface + '" name="outgoing_plr_' + interface + '" value=' + (json.INTERFACES[i].OUTGOING.PACKET_LOSS) + ' placeholder="Packet Loss (%)" min="0" step="0.1" data-bind="value:replyNumber">';
                        content += '</div>';
                        content += '</div>';


                        content += '<div class="row">';
                        content += '<div class="output col-sm-2"><p> Bandwidth limit </p> </div>';
                        content += '<div class="output col-sm-5">';
                        if (json.INTERFACES[i].hasOwnProperty("INCOMING")) {
                            content += '<input type="number" class="form-control" id="incoming_rate_' + interface + '" name="incoming_rate_' + interface + '" value=' + (json.INTERFACES[i].INCOMING.RATE_LIMIT) + ' placeholder="Bandwidth limit (Kbps)" min="0" step="100" data-bind="value:replyNumber">';
                        } else {
                            content += '<input type="number" class="form-control" id="incoming_rate_' + interface + '" name="incoming_rate_' + interface + '" value=0 disabled>';
                        }

                        content += '</div>';
                        content += '<div class="output col-sm-5">';
                        content += '<input type="number" class="form-control" id="outgoing_rate_' + interface + '" name="outgoing_rate_' + interface + '" value=' + (json.INTERFACES[i].OUTGOING.RATE_LIMIT) + ' placeholder="Bandwidth limit (Kbps)" min="0" step="100" data-bind="value:replyNumber">';
                        content += '</div>';
                        content += '</div>';

                        content += '<div class="row">';
                        content += '<div class="output col-sm-2"> </div>';
                        content += '<div class="output col-sm-5 form-group">';
                        content += '<button type="submit" class="btn btn-primary form-control" id="iface_rule_' + interface + '">Apply rules</button>';
                        content += '</div>';
                        content += '<div class="output col-sm-5 form-group">';
                        content += '<button type="submit" class="btn btn-secondary  form-control" id="del_iface_rule_' + interface + '">Reset rules</button>';
                        content += '</div>';
                        content += '</div>';

                        content += '</div>';

                        content += '<h1 class="section-subtitle">Profiles</h1>';


                        content += '<div class="row">';
                        content += '<div class="output col-sm-6">';
                        content += '<select id="profiles_' + interface + '" class="form-control">';
                        content += '<option>Select profile</option>';
                        profiles_json = JSON.parse(profiles.responseText);
                        for (var j in profiles_json.PROFILES) {
                            content += '<option>' + profiles_json.PROFILES[j].NAME + '</option>';
                        }
                        content += '</select>';
                        content += '</div>';
                        content += '<div class="output col-sm-6 form-group">';
                        content += '<button type="submit" class="btn btn-primary  form-control" id="load_profiles_' + interface + '">Load profile</button>';
                        content += '</div>';
                        content += '</div>';

                        content += '<div class="row">';
                        content += '<div class="output col-sm-6">';
                        content += '<input type="text" class="form-control" id="profile_name_' + interface + '" name="profile_name_' + interface + '"  placeholder="Profile_name">';
                        content += '</div>';
                        content += '<div class="output col-sm-6 form-group">';
                        content += '<button type="submit" class="btn btn-secondary  form-control" id="save_profiles_' + interface + '">Save current configuration as</button>';
                        content += '</div>';
                        content += '</div>';

                        content += '<br><br>';

                        content += '</div>';

                    }

                    $("#interfaces").html(tabs);
                    $("#interfaces-rules").html(content);

                    for (var k in json.INTERFACES) {
                        (function () {
                            var interface = json.INTERFACES[k].INTERFACE;

                            $('#load_profiles_' + interface).button().click(function () {
                                loadProfiles(interface);
                            });

                            $('#iface_rule_' + interface).button().click(function () {
                                sendNewRules(interface);
                            });

                            $('#del_iface_rule_' + interface).button().click(function () {
                                resetRulesInterfaces(interface);
                            });

                            $('#save_profiles_' + interface).button().click(function () {
                                saveProfiles(interface);
                            });
                        })();
                    }
                }
            };
        }
    };
}
