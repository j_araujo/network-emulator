# Codavel's Network Emulator

## Emulator API

Emulator API is a REST API that allows the configuration of the Emulator core service.
 

## Bulding

To build the API, access the root folder of the network emulator project and run:

``` bash
cmake . && make && make install
```

This will compile the core that allows the API to communicate with the core service, and copy the lib and header files to
_build/lib_ and _build/include_.

Then, copy the two folders to rest_api/emulator and, inside the folder rest_api, run:

``` bash
npm install
npm run build
```

Thios command will create the native modules required by the Node JS service.

## Running

To start the REST API, enter the folder rest_api and run

 ``` bash
npm start
 ```  
 
This will start the REST API on port 8888 and also the emulator service.
If you only want to start the API (and start the emulator through the daemon) run

``` bash
npm run start-api-only
```

The REST API allows the following operations:

* Get current configurations `GET /config`
* Send new configuration `POST /config`
* Get current profiles `GET /profiles`
* Save new profile `POST /profile`


## Examples

* Get a list of current network configuration

```bash
$ curl http://192.168.13.187:8888/config

{
    "INTERFACES": [
        {"INTERFACE":"eth0",
         "INCOMING":{"LATENCY":0,"PACKET_LOSS":0.1,"RATE_LIMIT":0},
         "OUTGOING":{"LATENCY":0,"PACKET_LOSS":0.0,"RATE_LIMIT":1000}
        },
        {"INTERFACE":"eth1",
         "INCOMING":{"LATENCY":100,"PACKET_LOSS":0.0,"RATE_LIMIT":0},
         "OUTGOING":{"LATENCY":0,"PACKET_LOSS":0.0,"RATE_LIMIT":0}
         }
    ],
    "STATUS":"OK"
}

```

* Add 100ms to outgoing traffic and 0.1% of packet loss to interface eth0

```bash
$ curl -X POST -i -H 'Content-Type: application/json' http://192.168.13.187:8888/config -d  '{"INTERFACE": "eth1", "INCOMING": {"PACKET_LOSS": 0.1}, "OUTGOING": {"LATENCY" : 100}}'

HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: text/html; charset=utf-8
Content-Length: 16
ETag: W/"10-EKFe2PUZe7ocISJplDqbnB2KYDI"
Date: Fri, 26 Oct 2018 09:28:46 GMT
Connection: keep-alive

{"STATUS": "OK"}

```

* Get a list of available profiles

```bash
$ curl http://192.168.13.187:8888/profiles

{
    "PROFILES": [
        {"NAME":"Profile_1",
         "INCOMING":{"LATENCY":100,"PACKET_LOSS":0.5,"RATE_LIMIT":1000},
         "OUTGOING":{"LATENCY":0,"PACKET_LOSS":0.0,"RATE_LIMIT":0}
         },
         {"NAME":"Profile_2",
          "INCOMING":{"LATENCY":2,"PACKET_LOSS":0.5,"RATE_LIMIT":2222},
          "OUTGOING":{"LATENCY":200,"PACKET_LOSS":0.100000001490116,"RATE_LIMIT":0}
          }
    ],
    "STATUS":"OK"
}
```

* Create a new profile that limits incoming traffic rate at 1000Kbit and the outgoing traffic at 2000Kbit


```bash
curl -X POST -i -H 'Content-Type: application/json' http://192.168.13.187:8888/profile -d  '{"NAME": "new profile", "INCOMING": {"RATE": 1000}, "OUTGOING": {"RATE" : 2000}}'

HTTP/1.1 200 OK
X-Powered-By: Express
Content-Type: text/html; charset=utf-8
Content-Length: 16
ETag: W/"10-EKFe2PUZe7ocISJplDqbnB2KYDI"
Date: Fri, 26 Oct 2018 09:54:09 GMT
Connection: keep-alive

{"STATUS": "OK"}
```

The new profile will be saved in the folder _profiles/_, with the name _new_profile.json_.