/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

'use strict';

const bodyParser = require('body-parser');
const express = require('express');
var app = express();

const emulator = require('../build/Release/native.node');
module.exports = emulator;

app.listen(8888);
app.use(bodyParser.json({limit: '50mb'}));
app.use(express.static('html'));

app.get('/config', function (req, res) {
    var json = {};
    json["COMMAND"] = 0;
    res.send(emulator.sendMessage(JSON.stringify(json)));
});

app.post('/config', function (req, res) {
    var json = {};
    json["COMMAND"] = 1;
    json["RULES"] = req.body;
    res.send(emulator.sendMessage(JSON.stringify(json)));
});

app.get('/profiles', function (req, res) {
    var json = {};
    json["COMMAND"] = 3;
    res.send(emulator.sendMessage(JSON.stringify(json)));
});

app.post('/profile', function (req, res) {
    var json = {};
    json["COMMAND"] = 4;
    json["PROFILE"] = req.body;
    res.send(emulator.sendMessage(JSON.stringify(json)));
});