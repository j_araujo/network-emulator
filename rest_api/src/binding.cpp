/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

#include <napi.h>
#include <Emulator.h>

/**
 * Sends a message to the emulator daemon
 * @param info argument info
 * @return a string containing the emulator's reply
 */
Napi::String SendMessage(const Napi::CallbackInfo &info) {
	Napi::Env env = info.Env();
	std::string ipc_sock = DEFAULT_IPC_SOCK;
	std::string json = info[0].As<Napi::String>().Utf8Value();
	std::string message_from_daemon = network_emulator::Emulator::sendMessageToEmulator(ipc_sock.c_str(), json.c_str());

	Napi::String returnValue = Napi::String::New(env, message_from_daemon);
	return returnValue;
}


/**
 * Starts the emulator service
 * @param info argument info
 */
void RunEmulator(const Napi::CallbackInfo &info) {
	network_emulator::Emulator emulator;

	if (info.Length() == 2) {
		std::string incoming_interface_arg = info[0].As<Napi::String>().Utf8Value();
		std::string outgoing_interface_arg = info[1].As<Napi::String>().Utf8Value();
		const char *incoming_interface = incoming_interface_arg.c_str();
		const char *outgoing_interface = outgoing_interface_arg.c_str();
		emulator.startEmulator(false, nullptr, nullptr, incoming_interface, outgoing_interface);
	} else {
		emulator.startEmulator(false, nullptr, nullptr, nullptr, nullptr);
	}
}

Napi::Object InitAll(Napi::Env env, Napi::Object exports) {
	exports.Set(
		"sendMessage", Napi::Function::New(env, SendMessage)
	);

	exports.Set(
		"runEmulator", Napi::Function::New(env, RunEmulator)
	);

	return exports;
}

NODE_API_MODULE(native, InitAll
)
