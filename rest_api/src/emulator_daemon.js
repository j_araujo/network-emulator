/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

'use strict';

const TRACE = false;

const args = process.argv.slice(2);
if (TRACE) {
    console.log(args);
}

const emulator = require('../build/Release/native.node');
module.exports = emulator;

if (args.length > 0) {
    if (TRACE) {
        console.log('using args');
    }
    emulator.runEmulator(args[0], args[1]);
} else {
    if (TRACE) {
        console.log('not using args');
    }
    emulator.runEmulator();
}


