#!/bin/bash

iptables -t nat -A PREROUTING -i $1 -p $2 --dport $3 -j DNAT --to-destination $4
