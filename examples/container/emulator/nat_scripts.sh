#!/bin/bash

iptables -A FORWARD -i $1 -j ACCEPT
iptables -A FORWARD -o $1 -j ACCEPT
iptables -t nat -A POSTROUTING -o $2 -j MASQUERADE